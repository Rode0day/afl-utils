"""
Copyright 2015-2021 @_rc0r <hlt99@blinkenshell.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from setuptools import setup
from setuptools import find_packages

import afl_utils

dependencies = [
    'exploitable==1.34',            # needed for gdb script execution
    'requests',                     # needed for stats submission
    'simplejson',                   # needed for config files
]

dependency_links = [
    'https://gitlab.com/wideglide/exploitable/-/archive/master/exploitable-master.tar.gz#egg=exploitable-1.34'
]

setup(
    name='afl-utils',
    version=afl_utils.__version__,
    packages=find_packages(),
    url='https://gitlab.com/rc0r/afl-utils',
    license='Apache License 2.0',
    author=afl_utils.__author_name__,
    author_email=afl_utils.__author_email__,
    description='Utilities for automated crash sample processing/analysis, easy afl-fuzz job management and corpus '
                'optimization',
    install_requires=dependencies,
    dependency_links=dependency_links,
    platforms=[
        'Any',
    ],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Operating System :: Linux',
        'Operating System :: Unix',
        'Programming Language :: Python :: 3.4',
        'Topic :: Fuzzing Utilities',
    ],
    entry_points={
        'console_scripts': [
            'afl-collect = afl_utils.afl_collect:main',
            'afl-cron = afl_utils.afl_cron:main',
            'afl-drcov = afl_utils.afl_drcov:main',
            'afl-fast_cov = afl_utils.afl_fast_cov:main',
            'afl-ccov = afl_utils.afl_ccov:main',
            'afl-gcov = afl_utils.afl_gcov:main',
            'afl-minimize = afl_utils.afl_minimize:main',
            'afl-multicore = afl_utils.afl_multicore:main',
            'afl-multikill = afl_utils.afl_multikill:main',
            'afl-stats = afl_utils.afl_stats:main',
            'afl-sync = afl_utils.afl_sync:main',
            'afl-vcrash = afl_utils.afl_vcrash:main'
        ]
    },
    keywords=[
        'Fuzzing',
        'American Fuzzy Lop',
    ],
    test_suite='tests'
)
