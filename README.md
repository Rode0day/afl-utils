# afl-utils [![pipeline status](https://gitlab.com/Rode0day/afl-utils/badges/master/pipeline.svg)](https://gitlab.com/Rode0day/afl-utils/commits/master)[![coverage report](https://gitlab.com/Rode0day/afl-utils/badges/master/coverage.svg)](https://gitlab.com/Rode0day/afl-utils/commits/master)

afl-utils is a collection of utilities to assist fuzzing with
[american-fuzzy-lop (afl)](http://lcamtuf.coredump.cx/afl/).
afl-utils includes tools for:

* automated crash sample collection, verification, reduction and analysis (`afl-collect`, `afl-vcrash`)
* easy management of parallel (multi-core) fuzzing jobs (`afl-multicore`, `afl-multikill`)
* corpus optimization (`afl-minimize`)
* fuzzer stats supervision (`afl-stats`)
* fuzzer queue synchronisation (`afl-sync`)
* autonomous utility execution (`afl-cron`)

**Added utilties**
* fast corpus coverage measurement (w/parallel execution)
    * basic block and edge coverage via QEMU (`afl-fast-cov`)
    * GCC gcov / lcov source base coverage (`afl-gcov`)
    * Clang source based code coverage (`afl-ccov`)
    * Dynamorio block coverage (`afl-drcov`)
* stats integration with [FuzzWrangler](https://gitlab.com/Rode0day/FuzzWrangler).

Various [screenshots](docs/README.md) of the tools in action can be found [docs](docs).

**For installation instructions see [docs/INSTALL.md](https://gitlab.com/Rode0day/afl-utils/blob/master/docs/INSTALL.md).**

A fuzzing workflow assisted by afl-utils:

```mermaid
graph LR
    A[config-file.json] -->|afl-multicore start| F{fuzzing}
    M -->|afl-mulicore resume| F
    D -->|afl-multicore resume| F
    F -->|afl-multikill stop| D(done)
    D -->|afl-minimize| M[min corpus]
    D -->|afl-collect| C[crash corpus]
    D -->|afl-gcov| G[gcov coverage]
    D -->|afl-ccov| CL[clang coverage]
    D -->|afl-stats| S
    F -->|afl-stats| S[stats]
```


