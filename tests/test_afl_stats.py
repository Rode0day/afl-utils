from afl_utils import afl_stats

import json
import os
import socket
import unittest
from db_connectors import con_sqlite

test_conf_settings = {
    'twitter_creds_file': '.afl-stats.creds',
    'twitter_consumer_key': 'your_consumer_key_here',
    'twitter_consumer_secret': 'your_consumer_secret_here',
    'fuzz_dirs': [
        '/path/to/fuzz/dir/0',
        '/path/to/fuzz/dir/1',
        'testdata/sync'
    ]
}

test_stats = {
    'pending_total': '0',
    'paths_favored': '25',
    'pending_favs': '0',
    'execs_per_sec': '1546.82',
    'fuzzer_pid': 0,
    'paths_total': '420',
    'unique_crashes': '0',
    'execs_done': '372033733',
    'afl_banner': 'target_000',
    'unique_hangs': '13'
}

test_complete_stats = {
    'pending_total': '0',
    'paths_favored': '25',
    'pending_favs': '0',
    'execs_per_sec': 336377.6971066908,
    'fuzzer_pid': '9999999',
    'paths_total': '420',
    'unique_crashes': '0',
    'execs_done': '372033733',
    'afl_banner': 'target_000',
    'unique_hangs': '13',
    'start_time': '1475080844',
    'last_update': '1475081950',
    'cycles_done': '0',
    'paths_found': '0',
    'paths_imported': '83',
    'max_depth': '39',
    'cur_path': '650',
    'variable_paths': '0',
    'stability': '100.00',
    'bitmap_cvg': '18.37',
    'last_path': '1475081940',
    'last_crash': '0',
    'last_hang': '0',
    'execs_since_crash': '63393',
    'exec_timeout': '800',
    'afl_version': '2.35b',
    'command_line': 'afl-fuzz -T target_000 -i ./in -o ./out -- ./target',
    'alive': 0,
    'runtime': 1106,
    'peak_rss_mb': '20',
    'slowest_exec_ms': '30'
}

test_sum_stats = {
    'fuzzers': 2,
    'pending_total': 0,
    'paths_favored': 50.0,
    'pending_favs': 0,
    'execs_per_sec': 336377.6971066908 * 2,
    'alive': 0.0,
    'paths_total': 840.0,
    'unique_crashes': 0,
    'execs_done': 372033733.0 * 2,
    'afl_banner': 'target_000',
    'total_runtime': 1106,
    'unique_hangs': 26.0,
    'host': socket.gethostname()[:10]
}

test_diff_stats = {
    'fuzzers': 1,
    'pending_total': 0,
    'paths_favored': 25.0,
    'pending_favs': 0,
    'execs_per_sec': 336377.6971066908,
    'alive': 0.0,
    'paths_total': 420.0,
    'unique_crashes': 0,
    'execs_done': 372033733.0,
    'afl_banner': 'target_000',
    'total_runtime': 553,
    'unique_hangs': 13.0,
    'host': socket.gethostname()[:10]
}

zero_stats = {
    'fuzzers': 0,
    'alive': 0,
    'execs_done': 0,
    'execs_per_sec': 0,
    'paths_total': 0,
    'paths_favored': 0,
    'pending_favs': 0,
    'pending_total': 0,
    'unique_crashes': 0,
    'unique_hangs': 0,
    'afl_banner': "",
    'total_runtime': 0,
    'host': socket.gethostname()[:10]
}

mon_stats = {"mon_blocks": 3747, "mon_edges": 5516, "mon_bugs": 35}

crash_stats = {
    "crashes_in": 10,
    "crashes_out": 10,
    "invalid": 0,
    "timeout": 0,
    "major_remain": 0,
    "hash_remove": 0
}

mon_plot = """rt,blocks,edges,bugs
0,2024,2815,0
1,2421,3441,0
2,2645,3822,0
3,2650,3834,0
4,2694,3901,0
5,2695,3903,0
6,2709,3925,0
9,2714,3934,0
10,2715,3936,0"""


class AflStatsTestCase(unittest.TestCase):
    def setUp(self):
        # Use to set up test environment prior to test case
        # invocation
        self.write_stats_file(mon_stats, './testdata/sync/monitor_stats.json')
        self.write_stats_file(crash_stats, './testdata/sync/crash_stats.json')
        self.write_file(mon_plot, './testdata/sync/monitor_plot')

    def tearDown(self):
        # Use for clean up after tests have run
        self.clean_remove('./testdata/afl-stats.db')
        self.clean_remove('./testdata/afl-stats2.db')
        self.clean_remove('./testdata/sync/monitor_plot')
        self.clean_remove('./testdata/sync/monitor_stats.json')
        try:
            os.remove('.afl_stats.sync')
        except FileNotFoundError:
            pass

    def clean_remove(self, file):
        if os.path.exists(file):
            os.remove(file)

    def write_stats_file(self, data, filepath):
        with open(filepath, 'w') as fp:
            json.dump(data, fp)

    def write_file(self, data, filepath):
        with open(filepath, 'w') as fp:
            fp.write(data)

    def test_read_config(self):
        conf_settings = afl_stats.read_config('testdata/afl-stats.conf.test')

        self.assertDictEqual(conf_settings, test_conf_settings)

        with self.assertRaises(SystemExit):
            afl_stats.read_config('/config-file-not-found')
        with self.assertRaises(SystemExit):
            afl_stats.read_config('testdata/afl-stats.conf.invalid02.test')

    def test_shorten_tweet(self):
        tw_in = 'A' * 140
        self.assertEqual(tw_in, afl_stats.shorten_tweet(tw_in))
        tw_in = 'ABCDEFGHIJ' * 14
        tw_in += 'xyz'
        tw_out = 'ABCDEFGHIJ' * 13
        tw_out += 'ABCDEFG...'
        self.assertEqual(tw_out, afl_stats.shorten_tweet(tw_in))

    def test_fuzzer_alive(self):
        mypid = os.getpid()
        invalidpid = -7
        self.assertEqual(1, afl_stats.fuzzer_alive(mypid))
        self.assertEqual(0, afl_stats.fuzzer_alive(invalidpid))

    def test_parse_stat_file(self):
        self.maxDiff = None
        self.assertIsNone(afl_stats.parse_stat_file('invalid-stat-file'))
        self.assertDictEqual(test_complete_stats, afl_stats.parse_stat_file('testdata/sync/fuzz000/fuzzer_stats'))
        self.assertIsNone(afl_stats.parse_stat_file('testdata/empty_fuzzer_stats'))

    def test_load_stats(self):
        self.assertEqual([], afl_stats.load_stats('invalid-fuzzer-dir'))
        fuzzer_stats = [
            test_complete_stats,
            test_complete_stats
        ]
        self.assertEqual([test_complete_stats], afl_stats.load_stats('testdata/sync/fuzz000'))
        self.assertEqual(fuzzer_stats, afl_stats.load_stats('testdata/sync'))

    def test_summarize_stats(self):
        stats = [
            test_complete_stats,
            test_complete_stats
        ]
        self.assertDictEqual(test_sum_stats, afl_stats.summarize_stats(stats))

    def test_diff_stats(self):
        self.assertDictEqual(test_diff_stats, afl_stats.diff_stats(test_sum_stats, test_diff_stats))
        corrupt_diff_stats = test_diff_stats.copy()
        corrupt_diff_stats.pop('host')
        self.assertDictEqual(afl_stats.diff_stats(test_sum_stats, corrupt_diff_stats), zero_stats)

    def test_prettify_stat(self):
        # ok, this is somewhat cheating...
        self.assertIsNotNone(afl_stats.prettify_stat(test_sum_stats, test_diff_stats, True))
        self.assertIsNotNone(afl_stats.prettify_stat(test_sum_stats, test_diff_stats, False))

        other_sum_stats = test_sum_stats.copy()
        other_sum_stats['alive'] = 1

        other_diff_stats = {
            'fuzzers': 0,
            'pending_total': 1,
            'paths_favored': 25.0,
            'pending_favs': 1,
            'execs_per_sec': 0,
            'alive': 0.0,
            'paths_total': 420.0,
            'unique_crashes': 1,
            'execs_done': 0,
            'afl_banner': 'target_000',
            'unique_hangs': 13.0,
            'host': socket.gethostname()[:10]
        }
        self.assertIsNotNone(afl_stats.prettify_stat(other_sum_stats, other_diff_stats))

        other_diff_stats = {
            'fuzzers': -1,
            'pending_total': 1,
            'paths_favored': 25.0,
            'pending_favs': 1,
            'execs_per_sec': -1,
            'alive': 0.0,
            'paths_total': 420.0,
            'unique_crashes': -1,
            'execs_done': -10,
            'afl_banner': 'target_000',
            'unique_hangs': 13.0,
            'host': socket.gethostname()[:10]
        }
        self.assertIsNotNone(afl_stats.prettify_stat(test_sum_stats, other_diff_stats))

    def test_dump_stats(self):
        config_settings = {'fuzz_dirs': ['./testdata/sync/']}
        lite_db = con_sqlite.sqliteConnector('./testdata/afl-stats.db', verbose=True)
        self.assertIsNone(afl_stats.dump_stats(config_settings, lite_db))
        self.assertTrue(os.path.exists('./testdata/afl-stats.db'))

    def test_fetch_stats(self):
        config_settings = afl_stats.read_config('testdata/afl-stats.conf.test')
        twitter_inst = None

        self.assertIsNone(afl_stats.fetch_stats(config_settings, twitter_inst))
        # os.remove('.afl_stats.sync')
        self.assertIsNone(afl_stats.fetch_stats(config_settings, twitter_inst))

    def test_main(self):
        with self.assertRaises(SystemExit) as se:
            afl_stats.main(['--twitter', '--config', './testdata/afl-stats.conf.test'])
        self.assertEqual(se.exception.code, 1)

    def test_main_no_twitter(self):
        args = ['--config', './testdata/afl-stats.conf.test2', '--database', 'testdata/afl-stats2.db']
        afl_stats.main(args)
        self.assertTrue(os.path.exists('testdata/afl-stats2.db'))
