from afl_utils import afl_gcov

import os
import shutil
import subprocess
import unittest

test_conf_settings = {
    'afl_margs': '-T banner',
    'fuzzer': 'afl-fuzz',
    'dumb': True,
    'timeout': '200+',
    'dict': 'dict/target.dict',
    'interactive': True,
    'target': './testdata/crash_process/bin/crash',
    'gcov_target': './testdata/crash_process/gcov/crash',
    'gcov_src_dir': './testdata/crash_process',
    'input': './in',
    'cmdline': '',
    'session': 'SESSION',
    'output': './testdata/output/sync',
    'mem_limit': '150',
    'master_instances': 2,
    'environment': [
        'AFL_PERSISTENT=1'
    ]
}


class AflStatsTestCase(unittest.TestCase):
    def setUp(self):
        # Use to set up test environment prior to test case
        # invocation
        self.init_queue_dir('testdata/sync/fuzz000/queue')
        self.init_queue_dir('testdata/sync/fuzz001/queue')
        subprocess.call(['make', '-C', 'testdata/crash_process'])
        subprocess.call(['make', '-C', 'testdata/crash_process', 'gcov'])

    def tearDown(self):
        # Use for clean up after tests have run
        self.clean_remove_dir('testdata/crash_process/bin')
        self.clean_remove_dir('testdata/crash_process/gcov')
        self.clean_remove_dir('testdata/sync/cov')
        self.clean_remove('testdata/crash_process/crash.gcda')
        self.clean_remove('testdata/crash_process/crash.gcno')

    def init_queue_dir(self, fuzzer_dir):
        self.clean_remove_dir(fuzzer_dir)
        shutil.copytree('testdata/queue', fuzzer_dir)

    def clean_remove(self, file):
        if os.path.exists(file):
            os.remove(file)

    def clean_remove_dir(self, dir):
        if os.path.exists(dir):
            shutil.rmtree(dir)

    def test_coverage(self):
        args = ['-c', 'testdata/afl-gcov.conf.test', '--web', '-f']
        self.assertIsNone(afl_gcov.main(args))
        self.assertTrue(os.path.exists('testdata/sync/cov/gcov_stats.json'))
        self.assertTrue(os.path.exists('testdata/sync/cov/web/index.html'))

    def test_main(self):
        with self.assertRaises(SystemExit):
            self.assertIsNone(afl_gcov.main())

        args = ['-c', 'testdata/afl-multicore.conf.test']
        self.assertIsNone(afl_gcov.main(args))
