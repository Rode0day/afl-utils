from afl_utils import afl_ccov

import unittest


class AflStatsTestCase(unittest.TestCase):
    def setUp(self):
        # Use to set up test environment prior to test case
        # invocation
        pass

    def tearDown(self):
        # Use for clean up after tests have run
        pass

    def test_main(self):
        with self.assertRaises(SystemExit):
            self.assertIsNone(afl_ccov.main())
