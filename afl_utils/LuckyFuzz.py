import binascii
from datetime import datetime
import hashlib
import io
import json
import logging
from itertools import islice
import os
import platform
import requests
import shutil
from subprocess import Popen, PIPE
from zipfile import ZipFile

from afl_utils.common import load_json_file, save_json_data

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def safe_json(text):
    try:
        j = json.loads(text)
    except ValueError:
        logger.error("Invalid response from server:\n\t{}".format(text))
        return {'success': False}
    return j


def get_signal(filename, default=None):
    if 'sig' in filename:
        return int(filename.split('sig:')[1].split(',')[0]) + 128
    return default


def generate_secret():
    return binascii.hexlify(os.urandom(16)).decode('ascii')


def sha1hexdigest(filepath):
    BLOCKSIZE = 65536
    m = hashlib.sha1()
    with open(filepath, 'rb') as fd:
        data = fd.read(BLOCKSIZE)
        while len(data) > 0:
            m.update(data)
            data = fd.read(BLOCKSIZE)
    return m.hexdigest()


def tmux_capture_console(session_name, pane_name, logpath):
    # tmux list-pane -s -F '#D #W' -t ph7
    # p = Popen(['tmux','list-pane','-sF',"'#D #W'",'-t',job['session']], stdout=PIPE, encoding='utf-8')
    p = Popen(['tmux', 'list-pane', '-sF', "'#D #W'", '-t', session_name],
              stdout=PIPE, stderr=PIPE, universal_newlines=True)
    out, err = p.communicate()
    if pane_name in out:
        out = out.replace("'", "").split('\n')
        pane_id = [w.split()[0] for w in out if pane_name in w][0]
        if len(pane_id) < 2:
            return
        # tmux capture-pane -ep -t %4 -E 28 > output/ph7000/console.log
        p = Popen('tmux capture-pane -ept {} -E 28 > {}/console.log'.format(pane_id, logpath), shell=True)
        p.wait()
        if shutil.which('ansi2html.sh'):
            p = Popen('ansi2html.sh > {}/console.html < {}/console.log'.format(logpath, logpath), shell=True)
            p.wait()


def reduce_file(file_path, max_lines=9999):
    tfile = io.BytesIO()
    f = open(file_path, 'rb')
    lines = sum(1 for line in f)
    f.seek(0)
    i = 0
    mod_lines = lines // max_lines + 1
    for line in f:
        if i % mod_lines == 0 or i == 1:
            tfile.write(line)
        i += 1
    f.close()
    tfile.seek(0)
    return tfile


class LuckyFuzzClient(object):

    def __init__(self, settings):
        self.status = False
        self.cache = dict()
        self.HOST = platform.node()

        self.cache_file = os.path.join(settings['output'], '.cache')
        self.load_cache(self.cache_file)
        self.load_cache(os.path.abspath(os.path.expanduser('~/.luckyfuzz')))
        self.load_cache('.luckyfuzz')
        if 'LuckyFuzz_url' not in self.cache:
            logger.error("LuckyFuzz_url not found.")
            return None
        if 'LuckyFuzz_token' not in self.cache:
            logger.error("LuckyFuzz_url not found.")
            return None
        self.base_url = self.cache['LuckyFuzz_url']
        self.auth_token = self.cache['LuckyFuzz_token']
        self.job = settings
        if 'job_secret' not in self.cache:
            force = settings.get('submit_force')
            if self.submit_job(force=force) is None:
                return None
        self.status = True

    def load_cache(self, cache_file):
        load_json_file(cache_file, data=self.cache, fail=False)

    def save_cache(self):
        sanitized = self.cache.copy()
        sanitized.pop('LuckyFuzz_token', None)
        sanitized.pop('LuckyFuzz_url', None)
        sanitized.pop('r0_api_token', None)
        save_json_data(sanitized, self.cache_file, indent=4)

    def submit_crash(self, sid, sample):
        cf = sample['input']
        url = self.base_url + '/api/crash/submit'
        ci = {'timestamp': datetime.utcfromtimestamp(os.path.getmtime(cf)),
              'job_secret': self.cache['job_secret'],
              'stat_id': sid,
              'crash_signal': get_signal(cf, sample.get('signal')),
              'bug_id': sample.get('bug_id'),
              'exploitability': sample.get('exploitability'),
              'stack_hash': sample.get('stack_hash', '_._'),
              'additional': sample.get('additional')}
        if 'r0_api_token' in self.cache:
            ci['r0_api_token'] = self.cache['r0_api_token']
        files = {'input': open(cf, 'rb')}
        r = requests.post(url, headers=self.auth_token, data=ci, files=files)
        return safe_json(r.text)

    def submit_crashes(self, index):
        submitted = 0
        for sample in index:
            fuzzer = sample['fuzzer']
            result = self.submit_stats(fuzzer, exists=False)
            if result is False:
                continue
            sid = self.cache[fuzzer]['stat_id']
            j = self.submit_crash(sid, sample)
            if j and j['success']:
                submitted += 1
        return submitted

    def check_src_cov_stats(self, data, new):
        if 'l_ratio' in new:
            data.update(
                {'cov_line': new.get('l_ratio'),
                 'cov_func': new.get('f_ratio'),
                 'cov_branch': new.get('b_ratio')})

    def check_block_cov_stats(self, data, new):
        if 'skipped' in new and 'cov_bb' in new:
            data['cov_bb'] = new.get('cov_bb')
            data['cov_bbe'] = new.get('cov_edge')

    def submit_cov_stats(self, cov_stats):
        url = self.base_url + '/api/job/update/{job_id}'.format(**self.cache)
        job_update = {'job_secret': self.cache['job_secret']}
        self.check_src_cov_stats(job_update, cov_stats)
        self.check_block_cov_stats(job_update, cov_stats)
        logger.info("Submitting {} stats for job.".format(len(job_update)))

        r = requests.post(url, headers=self.auth_token, json=job_update)
        return safe_json(r.text)

    def submit_job_stats(self, sum_stats, cov_stats=None):
        url = self.base_url + '/api/job/update/{job_id}'.format(**self.cache)
        job_update = {'job_secret': self.cache['job_secret'],
                      'total_execs': sum_stats.get('execs_done'),
                      'total_paths': sum_stats.get('paths_total'),
                      'total_runtime': sum_stats.get('total_runtime'),
                      'alive': sum_stats.get('alive'),
                      'speed': sum_stats.get('execs_per_sec')}
        if cov_stats is not None:
            self.check_src_cov_stats(job_update, cov_stats)
            if 'mon_blocks' in cov_stats:
                for k in ['mon_blocks', 'mon_edges', 'mon_bugs']:
                    job_update[k] = cov_stats.get(k)
                job_update['cov_bb'] = cov_stats.get('mon_blocks')
                job_update['cov_bbe'] = cov_stats.get('mon_edges')
            self.check_block_cov_stats(job_update, cov_stats)

            job_update['cov_json'] = json.dumps(cov_stats)
        files = {'json': ('json', json.dumps(job_update), 'application/json')}
        mon_pd = os.path.join(self.job['output'], 'monitor_plot')
        if os.path.isfile(mon_pd):
            files['plot_data'] = ('monitor_plot', open(mon_pd, 'rb'), 'application/octet')
        r = requests.post(url, headers=self.auth_token, files=files)
        return safe_json(r.text)

    def submit_stats(self, fuzzer, exists=True, alive=False):
        fpath = os.path.join(self.job['output'], fuzzer)
        if fuzzer not in self.cache \
                or 'stat_id' not in self.cache[fuzzer]:
            j = self.submit_stats_file(fpath, alive=alive)
            if j and j['success']:
                self.cache[fuzzer] = {'stat_id': j['stat_id']}
                self.save_cache()
        elif exists:
            sid = self.cache[fuzzer]['stat_id']
            j = self.submit_stats_file(fpath, sid, alive=alive)
        else:
            return True
        return j and j['success']

    def submit_stats_file(self, fpath, sid=None, alive=False):
        if 'job_secret' not in self.cache:
            return None
        if not os.path.isdir(fpath):
            return None
        sf = os.path.join(fpath, 'fuzzer_stats')
        if not os.path.isfile(sf):
            logger.warning(" stat files not present in {}".format(fpath))
            return None
        files = {'stats_file': open(sf, 'rb')}

        pd = os.path.join(fpath, 'plot_data')
        if os.path.isfile(pd):
            pd_trim = reduce_file(pd)
            files['plot_data'] = pd_trim

        bm = os.path.join(fpath, 'fuzz_bitmap')
        if os.path.isfile(bm):
            files['fuzz_bitmap'] = open(bm, 'rb')

        url = self.base_url + '/api/stats/submit'

        instance = os.path.split(fpath)[1]
        tmux_capture_console(self.job['session'], instance, fpath)

        cl = os.path.join(fpath, 'console.log')
        cl_html = os.path.join(fpath, 'console.html')
        if os.path.isfile(cl_html):
            files['console.html'] = open(cl_html, 'rb')
        elif os.path.isfile(cl):
            files['console.log'] = open(cl, 'rb')
        form = {'stat_id': sid,
                'alive': alive,
                'job_secret': self.cache['job_secret'],
                'fuzzer_hostname': self.HOST}
        r = requests.post(url, headers=self.auth_token, data=form, files=files)
        for f in files.values():
            f.close()
        return safe_json(r.text)

    def submit_job(self, src=None, force=False):  # noqa C901
        if not os.path.isfile(self.job['target']):
            logger.error("[-] target file not found")
            return None
        if not os.path.isdir(self.job['output']):
            logger.error("[-] output/sync dir not found")
            return None
        if not os.path.isdir(self.job['input']):
            logger.error("[-] input/seed file(s) not found")
            return None
        if 'job_secret' in self.cache:
            job_secret = self.cache['job_secret']
        elif force:
            job_secret = generate_secret()
        else:
            s = input("\nPlease confirm you want to create a new job: %s? " % self.job['name'])
            if len(s) > 0 and (s[0] == 'y' or s[0] == 'Y'):
                job_secret = generate_secret()
            else:
                logger.error("Aborting submit_job by user request.")
                return None

        s_dir = self.job['input']
        seed_files = [f.path for f in islice(os.scandir(s_dir), 20) if f.is_file()]
        if len(seed_files) == 1:
            sf = seed_files[0]
        elif len(seed_files) > 1:
            sf = '.seeds.zip'
            with ZipFile(sf, 'w') as szip:
                [szip.write(f) for f in seed_files]

        self.job['target_hash'] = sha1hexdigest(self.job['target'])

        if 'src_archive' in self.job['target_info'] and \
                os.path.isfile(self.job['target_info']['src_archive']):
            src = self.job['target_info']['src_archive']
            logger.infot("  source archive: {}".format(src))
        else:
            src = None

        url = self.base_url + '/api/job'
        form = {'job_secret': job_secret}
        self.job['target_info']['filename'] = os.path.basename(self.job['target'])
        files = {
            'json': ('json', json.dumps(self.job), 'application/json'),
            'seeds': open(sf, 'rb'),
        }
        if os.stat(self.job['target']).st_size < (1024 * 1024 * 10):
            files['target'] = open(self.job['target'], 'rb')

        if src:
            files['src_archive'] = open(src, 'rb')
        r = requests.post(url, headers=self.auth_token, data=form, files=files)
        j = safe_json(r.text)
        if j['success']:
            self.cache['job_secret'] = job_secret
            self.cache.update(j)
            self.save_cache()
        return True
