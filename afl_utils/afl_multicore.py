"""
Copyright 2015-2021 @_rc0r <hlt99@blinkenshell.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import argparse
import os
import platform
import shlex
import shutil
import signal
import subprocess
import sys
import time

import afl_utils
from afl_utils.AflPrettyPrint import print_err, print_ok, show_info
from afl_utils.common import read_config, build_target_cmd


def find_fuzzer_binary(fuzzer_bin):
    afl_path = shutil.which(fuzzer_bin)
    if afl_path and os.path.isfile(afl_path):
        return fuzzer_bin
    print_err("Fuzzer binary not found!")
    sys.exit(1)


def afl_cmdline_from_config(config_settings, instance_number):
    afl_cmdline = [find_fuzzer_binary(config_settings["fuzzer"])]

    if "qemu" in config_settings and config_settings["qemu"]:
        afl_cmdline.append("-Q")

    if "input" in config_settings:
        afl_cmdline.extend(["-i", config_settings["input"]])

    if "output" in config_settings:
        afl_cmdline.extend(["-o", config_settings["output"]])

    if "file" in config_settings:
        afl_cmdline.append("-f")
        if config_settings["file"] != "@@":
            afl_cmdline.append(".%03d_%s" % (instance_number, config_settings["file"]))
        else:
            afl_cmdline.append(config_settings["file"])

    if "mem_limit" in config_settings:
        afl_cmdline.extend(["-m", config_settings["mem_limit"]])

    if "time_limit" in config_settings:
        afl_cmdline.extend(["-V", config_settings["time_limit"]])

    if "exec_limit" in config_settings:
        afl_cmdline.extend(["-E", config_settings["exec_limit"]])

    if "timeout" in config_settings:
        afl_cmdline.extend(["-t", "{timeout}".format(**config_settings)])

    if "dict" in config_settings:
        afl_cmdline.extend(["-x", config_settings["dict"]])

    if "cmplog" in config_settings:
        afl_cmdline.extend(["-c", config_settings["cmplog"]])

    if "dirty" in config_settings and config_settings["dirty"]:
        afl_cmdline.append("-d")

    if "dumb" in config_settings and config_settings["dumb"]:
        afl_cmdline.append("-n")

    if "afl_margs" in config_settings:
        afl_cmdline.extend(shlex.split(config_settings["afl_margs"]))

    return afl_cmdline


def setup_tmux(session_name, env_list, workdir):
    p = subprocess.Popen(['tmux', 'new-session', '-Pd',
                          '-s', session_name,
                          '-t', session_name,
                          '-c', workdir,
                          '-n', 'AFL_bash',
                          '-x', '80', '-y', '60',
                          '/bin/bash'],
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate()
    setw = ['tmux', 'set', '-g', 'remain-on-exit', 'on']
    subprocess.Popen(setw)

    if len(env_list) > 0:
        for env in env_list:
            env_tuple = env.split("=")
            tmux_env_cmd = ['tmux', 'set-environment', '-t', session_name, env_tuple[0], env_tuple[1]]
            subprocess.Popen(tmux_env_cmd)


def check_screen():
    inside_screen = False
    # try:
    #     screen_output = subprocess.check_output("screen -ls", shell=True, stderr=subprocess.DEVNULL,
    #                                             universal_newlines=True)
    # except (subprocess.TimeoutExpired, subprocess.CalledProcessError) as e:
    #     screen_output = e.output
    #
    # screen_output = screen_output.splitlines()
    #
    # for line in screen_output:
    #     if "(Attached)" in line:
    #         inside_screen = True

    if os.environ.get("STY"):
        inside_screen = True

    return inside_screen


def setup_screen_env(env_list):
    if len(env_list) > 0:
        # set environment variables in initializer window
        for env in env_list:
            env_tuple = env.split("=")
            screen_env_cmd = ["screen", "-X", "setenv", env_tuple[0], env_tuple[1]]
            subprocess.Popen(screen_env_cmd)

    # Set working directory for all newly
    # created screen windows to the directory
    # afl-multicore was executed from.
    cwd = os.getcwd()
    subprocess.Popen(["screen", "-X", "chdir", cwd])


def setup_screen(windows, env_list):
    setup_screen_env(env_list)

    # create number of windows
    for i in range(0, windows, 1):
        subprocess.Popen(["screen"])

    # go back to 1st window
    subprocess.Popen("screen -X select 0".split())


def sigint_handler(signal, frame):
    print()
    print_ok("Test run aborted by user!")
    sys.exit(0)


def build_master_cmd(settings, master_index, target_cmd):
    # If afl -f file switch was used, automatically use correct input
    # file for master instance.
    if "%%" in target_cmd:
        target_cmd = target_cmd.replace("%%", settings["file"] + "_%03d" % master_index)
    # compile command-line for master
    # $ afl-fuzz -i <input_dir> -o <output_dir> -M <session_name>.000 <afl_args> \
    #   </path/to/target.bin> <target_args>
    master_cmd = afl_cmdline_from_config(settings, master_index)
    if "master_instances" in settings and settings["master_instances"] > 1:
        # multi-master mode
        master_cmd += ["-M", "'%s%03d:%d/%d'" % (settings["session"], master_index,
                                                 master_index + 1, settings["master_instances"]), "--"]
    else:
        # single-master mode
        master_cmd += ["-M", "%s000" % settings["session"], "--"]
    return master_cmd + shlex.split(target_cmd)


def build_slave_cmd(settings, slave_index, target_cmd):
    # If afl -f file switch was used, automatically use correct input
    # file for slave instance.
    if "%%" in target_cmd:
        target_cmd = target_cmd.replace("%%", settings["file"] + "_%03d" % slave_index)
    # compile command-line for slaves
    # $ afl-fuzz -i <input_dir> -o <output_dir> -S <session_name>.NNN <afl_args> \
    #   </path/to/target.bin> <target_args>
    slave_cmd = afl_cmdline_from_config(settings, slave_index)
    slave_cmd += ["-S", "%s%03d" % (settings["session"], slave_index), "--"]
    return slave_cmd + shlex.split(target_cmd)


def write_pgid_file(settings):
    if "interactive" not in settings or not settings["interactive"]:
        # write/append PGID to file /tmp/afl-multicore.PGID.<SESSION>
        f = open("/tmp/afl_multicore.PGID.%s" % settings["session"], "a")
        if f.writable():
            f.write("%d\n" % os.getpgid(0))
        f.close()
        print_ok("For progress info check: %s/%sxxx/fuzzer_stats!" %
                 (settings["output"], settings["session"]))
    else:
        print_ok("Check the newly created tmux windows!")


def get_master_count(settings):
    if "master_instances" in settings:
        if settings["master_instances"] >= 0:
            return settings["master_instances"]
        else:
            return 0
    else:
        return 1


def get_started_instance_count(command, settings):
    instances_started = 0
    if command == "add":
        dirs = os.listdir(settings["output"])
        for d in dirs:
            if os.path.isdir(os.path.abspath(os.path.join(settings["output"], d))) \
                    and settings["session"] in d:
                instances_started += 1
    return instances_started


def get_job_counts(jobs_arg):
    if isinstance(jobs_arg, str) and "," in jobs_arg:
        jobs_arg = jobs_arg.split(",")
        num_jobs = int(jobs_arg[0])
        jobs_offset = int(jobs_arg[1])
    else:
        num_jobs = int(jobs_arg)
        jobs_offset = 0
    return (num_jobs, jobs_offset)


def has_master(settings, jobs_offset):
    return jobs_offset < get_master_count(settings)


def startup_delay(settings, instance_num, command, startup_delay):
    if startup_delay is not None:
        if startup_delay == "auto":
            if command == "resume":
                delay = auto_startup_delay(settings, instance_num)
            else:
                delay = auto_startup_delay(settings, 0, resume=False)
        else:
            delay = int(startup_delay)

        time.sleep(delay)
        return delay


def auto_startup_delay(settings, instance_num, resume=True):
    # Worst case startup time (t_sw) per fuzzer (N - number of samples, T - max. timeout):
    #   t_sw = N * T
    # Optimized case startup time (t_sa) per fuzzer (O - optimization factor):
    #   t_sa = O * t_sw = O * N * T
    # Educated guess for some O:
    #    O = 1 / sqrt(N)
    # This might need some tuning!
    if resume:
        instance_dir = os.path.join(settings["output"],
                                    "{}{:03d}".format(settings["session"], instance_num), "queue")
    else:
        instance_dir = settings["input"]
    sample_list = os.listdir(instance_dir)
    N = len(sample_list)
    T = float(settings["timeout"].strip(" +")) if "timeout" in settings else 1000.0
    _O = N**(-1 / 2)

    return _O * T * N / 1000


def main(argv=None):
    show_info("afl-multicore", afl_utils.__author__, "Wrapper script to easily set up parallel fuzzing jobs.")

    parser = argparse.ArgumentParser(description="afl-multicore starts several parallel fuzzing jobs, that are run \
in the background. For fuzzer stats see 'out_dir/SESSION###/fuzzer_stats'!",
                                     usage="afl-multicore [-c config] [-h] [-s secs] [-t] [-v] <cmd> <jobs[,offset]>")

    parser.add_argument("-c", "--config", dest="config_file",
                        help="afl-multicore config file (Default: afl-multicore.conf)!", default="afl-multicore.conf")
    parser.add_argument("-s", "--startup-delay", dest="startup_delay", default=None, help="Wait a configurable  amount \
of time after starting/resuming each afl instance to avoid interference during fuzzer startup. Provide wait time in \
seconds.")
    parser.add_argument("-t", "--test", dest="test_run", action="store_const", const=True, default=False, help="Perform\
 a test run by starting a single afl instance in interactive mode using a test output directory.")
    parser.add_argument("-v", "--verbose", dest="verbose", action="store_const", const=True,
                        default=False, help="For debugging purposes do not redirect stderr/stdout of the created \
subprocesses to /dev/null (Default: off). Check 'nohup.out' for further outputs.")
    parser.add_argument("cmd", help="afl-multicore command to execute: start, resume, add.")
    parser.add_argument("jobs", help="Number of instances to start/resume/add. For resumes you may specify an optional \
job offset that allows to resume specific (ranges of) afl-instances.")

    args = parser.parse_args(argv)

    # Read config file and change working dirctory to file location
    config_file_path = os.path.abspath(os.path.expanduser(args.config_file))
    settings = read_config(config_file_path)
    # os.chdir(os.path.dirname(config_file_path))

    if args.test_run:
        signal.signal(signal.SIGINT, sigint_handler)
        settings["output"] += "_test"
        settings["interactive"] = False
        args.jobs = 1
        args.cmd = "start"

    jobs_count, jobs_offset = get_job_counts(args.jobs)

    if args.cmd != "resume":
        # settings["input"] = os.path.abspath(os.path.expanduser(settings["input"]))
        jobs_offset = 0
        if not os.path.exists(settings["input"]):
            print_err("No valid directory provided for <INPUT_DIR>!")
            sys.exit(1)
    else:
        settings["input"] = "-"

    # settings["output"] = os.path.abspath(os.path.expanduser(settings["output"]))

    instances_started = get_started_instance_count(args.cmd, settings)
    master_count = get_master_count(settings)

    if "interactive" in settings and settings["interactive"]:
        env_list = settings['environment'] if 'environment' in settings else []
        work_dir = os.path.dirname(os.path.abspath(os.path.expanduser(args.config_file)))
        setup_tmux(settings['session'], env_list, work_dir)

    target_cmd = build_target_cmd(settings, settings.get('alt_target'))

    if args.test_run:
        cmd = build_master_cmd(settings, 0, target_cmd)
        with subprocess.Popen(cmd) as test_proc:
            print_ok("Test instance started (PID: %d)" % test_proc.pid)

    print_ok("Starting fuzzer instance(s)...")
    jobs_offset += instances_started
    jobs_count += jobs_offset
    for i in range(jobs_offset, jobs_count, 1):
        is_master = has_master(settings, i)
        instance_name = "{}{:03}".format(settings['session'], i)

        if is_master:
            cmd = build_master_cmd(settings, i, target_cmd)
        else:
            cmd = build_slave_cmd(settings, i, target_cmd)

        if 'docker' in settings:
            cmd = ['docker', 'run', '-it', '--rm',
                   '--name', instance_name,
                   '-v', os.getcwd() + ':/data',
                   '--pid=host',
                   '--hostname', platform.node() + '-docker-' + instance_name,
                   '--entrypoint', 'afl-fuzz',
                   settings['docker']] + cmd[1:]

        if "interactive" in settings and settings["interactive"]:
            work_dir = os.path.dirname(os.path.abspath(os.path.expanduser(args.config_file)))
            # create number of windows
            cmd = ["tmux", "new-window", '-ad',
                   '-t', settings['session'],
                   '-n', instance_name,
                   '-c', work_dir,
                   ] + cmd
            print(" Executing: %s" % ' '.join(cmd))
            subprocess.Popen(cmd)
            if is_master:
                if master_count == 1:
                    print(" Master %03d started inside new tmux window" % i)
                else:
                    print(" Master %03d/%03d started inside new tmux window" % (i, master_count - 1))
            else:
                print(" Slave %03d started inside new tmux window" % i)
        else:
            if not args.verbose:
                fuzzer_inst = subprocess.Popen(['nohup'] + cmd,
                                               stdout=subprocess.DEVNULL,
                                               stderr=subprocess.DEVNULL)
            else:
                fuzzer_inst = subprocess.Popen(['nohup'] + cmd)
            if is_master:
                if master_count == 1:
                    print(" Master %03d started (PID: %d)" % (i, fuzzer_inst.pid))
                else:
                    print(" Master %03d/%03d started (PID: %d)" % (i, master_count - 1, fuzzer_inst.pid))
            else:
                print(" Slave %03d started (PID: %d)" % (i, fuzzer_inst.pid))

        if i < (jobs_count - 1):
            startup_delay(settings, i, args.cmd, args.startup_delay)

    write_pgid_file(settings)


if __name__ == "__main__":
    main(sys.argv)
