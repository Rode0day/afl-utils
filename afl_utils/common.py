import os
import sys

try:
    import simplejson as json
except ImportError:
    import json

from afl_utils.AflPrettyPrint import print_warn, print_err


def load_json_file(filepath, data=None, fail=True, root=None):
    if data is None:
        data = dict()

    exists = os.path.isfile(filepath)
    if fail and not exists:
        print_err("file {} not found!".format(filepath))
        sys.exit(1)
    if not exists:
        return data

    try:
        with open(filepath, 'r') as json_file:
            if root is None:
                data.update(json.load(json_file))
            else:
                data[root] = json.load(json_file)
    except OSError:
        print_warn("Failed to read {}".format(filepath))
    except ValueError:
        print_warn("Invalid json in {}".format(filepath))
    return data


def save_json_data(data, filepath, **kwargs):
    try:
        with open(filepath, 'w') as json_file:
            json.dump(data, json_file, **kwargs)
    except OSError:
        print_warn("Failed to write {}".format(filepath))


def read_config(config_file):

    config = load_json_file(config_file)

    if "target" not in config:
        print_err("Invalid job config file")
        sys.exit(2)

    if "fuzzer" not in config:
        config["fuzzer"] = "afl-fuzz"

    if "session" not in config:
        config["session"] = "SESSION"

    if "master_instances" in config:
        config["master_instances"] = int(config["master_instances"])

    return config


def build_target_cmd(settings, alt_target=None):
    if isinstance(settings, dict):
        target = settings['target'] if alt_target is None else alt_target
        args = settings["cmdline"]
    elif isinstance(settings, list):
        target_cmdline = " ".join(settings).split()
        target = os.path.abspath(os.path.expanduser(target_cmdline[0]))
        args = " ".join(settings[1:])
    else:
        print_err("Error in  parsing target command!")
        sys.exit(1)
    if not os.path.exists(target):
        print_err("Target binary ({}) not found!".format(target))
        sys.exit(1)
    return ' '.join([target, args])
