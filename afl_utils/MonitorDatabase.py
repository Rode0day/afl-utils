import csv
import sqlite3


def dump_blocks(cur, experiment_id='1'):
    d = dict()
    stmt = ("SELECT reltime, COUNT(DISTINCT block_id) "
            "FROM foundblock WHERE experiment_id = ? "
            "GROUP BY reltime ORDER BY reltime")
    cur.execute(stmt, experiment_id)
    rows = cur.fetchall()
    c = 0
    for (ts, b) in rows:
        c += b
        d[ts] = c
    return d


def dump_edges(cur, experiment_id='1'):
    d = dict()
    stmt = ("SELECT reltime, COUNT(DISTINCT edge_id) "
            "FROM foundedge WHERE experiment_id = ? "
            "GROUP BY reltime ORDER BY reltime")
    cur.execute(stmt, experiment_id)
    rows = cur.fetchall()
    c = 0
    for (ts, e) in rows:
        c += e
        d[ts] = c
    return d


def dump_bugs(cur, experiment_id='1'):
    d = dict()
    stmt = ("SELECT MIN(f.reltime) AS f_reltime, l.bug_id "
            "FROM lavabug l LEFT JOIN foundbug f ON l.id = f.bug_id "
            "WHERE ABS(f.returncode) > 2 AND f.experiment_id = ? "
            "GROUP BY l.bug_id ORDER BY f_reltime")
    cur.execute(stmt, experiment_id)
    rows = cur.fetchall()
    c = 1
    for (ts, lb) in rows:
        d[ts] = c
        c += 1
    return d


def union_data(cur, plot_file):
    blocks = dump_blocks(cur)
    edges = dump_edges(cur)
    bugs = dump_bugs(cur)
    all_ts = set(blocks.keys())
    all_ts.update(set(edges.keys()))
    all_ts.update(set(bugs.keys()))
    row = {'rt': 0, 'blocks': 0, 'edges': 0, 'bugs': 0}
    fieldnames = list(row.keys())
    with open(plot_file, 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        writer.writeheader()
        for ts in sorted(all_ts):
            row['rt'] = ts
            if ts in blocks:
                row['blocks'] = blocks[ts]
            if ts in edges:
                row['edges'] = edges[ts]
            if ts in bugs:
                row['bugs'] = bugs[ts]
            writer.writerow(row)


def dump_plotdata(cur, plot_file):
    cur.execute("SELECT count(name) FROM sqlite_master WHERE type='table' AND name='plotdata'")
    if cur.fetchone()[0] == 0:
        return False
    stmt = "SELECT reltime AS rt,blocks,edges,bugs,crashes FROM plotdata"
    cur.execute(stmt)
    fieldnames = ['rt', 'blocks', 'edges', 'bugs', 'crashes']
    with open(plot_file, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(fieldnames)
        writer.writerows(cur.fetchall())
    return True


def process_monitor_db(db_filepath, plot_filepath):
    try:
        conn = sqlite3.connect(db_filepath)
        cur = conn.cursor()
        if not dump_plotdata(cur, plot_filepath):
            union_data(cur, plot_filepath)
        conn.close()
    except sqlite3.Error as e:
        print(e)
