"""
Copyright 2015-2021 @_rc0r <hlt99@blinkenshell.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from contextlib import suppress
import hashlib
import json
import os
import re
import resource
import shlex
from subprocess import Popen, STDOUT, PIPE, DEVNULL, TimeoutExpired, CalledProcessError

from afl_utils.AflPrettyPrint import clr, print_ok, print_warn, print_err  # noqa

import threading

# regex for bug_id
digits_re = re.compile(r'\D*(\d+).*')


def hash_list(stack):
    m = hashlib.md5()
    for frame in stack:
        m.update(frame.encode())
    return m.hexdigest()


def parse_asan_log(log):
    res = dict()
    asan_re1 = re.search(r"={65}\n(.*)\n==[0-9]+==ABORTING", log, flags=re.DOTALL)
    if asan_re1:
        asan_log = asan_re1.group(1)
        asan_re2 = re.search(r"==\d+==ERROR: (.*?)\n\n", log, flags=re.DOTALL)
        if asan_re2:
            asan_stack_trace = asan_re2.group(1)
            asan_re3 = re.findall(r"\s+(#\d+ .*)", asan_stack_trace)
            if len(asan_re3) > 0:
                res['stacktrace'] = asan_re3
                res['stackhash'] = hash_list(res['stacktrace'])
        asan_re4 = re.search(r"==\d+==ERROR: (\w+): (.*)", asan_log)
        if asan_re4:
            res['error_msg'] = asan_re4.group(2)
        asan_re5 = re.search(r"SUMMARY: (\w+):(.*)", asan_log)
        if asan_re5:
            res['summary'] = asan_re5.group(2)
    return res


def parse_lava_log(log):
    r = {'bug_id': None, 'src_line': ""}
    lava = log.rfind('LAVALOG:')
    msg = log[lava:].split(maxsplit=3)
    r['bug_id'] = msg[1].strip()[:-1]
    r['src_line'] = msg[2].split('\n')[0].strip()
    return r


def parse_magma_log(log, bug_marker='MAGMALOG:'):
    """Parse output for last bug_marker (rfind).
    This will be the triggered bug."""
    r = {'bug_id': None, 'src_line': ""}
    lava = log.rfind(bug_marker)
    msg = log[lava:].split(maxsplit=3)
    magma_bug_id = msg[1].strip()[:-1]
    r['bug_id'] = digits_re.match(magma_bug_id).group(1)
    r['src_line'] = magma_bug_id
    return r


def set_mem_limit(limit=1):
    soft, hard = resource.getrlimit(resource.RLIMIT_AS)
    memlimit = 2**30 * limit  # 1 GB
    resource.setrlimit(resource.RLIMIT_AS, (memlimit, memlimit))


class ExecThread(threading.Thread):
    def __init__(self, thread_id, timeout_secs, cmd_dict, in_queue, out_queue):
        threading.Thread.__init__(self)
        self.id = thread_id
        self.gdb_cmd = cmd_dict.get('gdb_cmd')
        self.tfile = ".triage_input{:06d}".format(thread_id)
        self.target_cmd = cmd_dict['target_cmd'].replace("@@", self.tfile)
        self.asan_cmd = None
        if isinstance(cmd_dict.get('asan_cmd'), str):
            self.asan_cmd = cmd_dict.get('asan_cmd').replace("@@", self.tfile)
        self.verify = cmd_dict.get('verify')
        self.use_stdin = "@@" not in cmd_dict['target_cmd']
        self.in_queue = in_queue
        self.out_queue = out_queue
        self.link = os.link
        if os.path.exists(self.tfile):
            os.unlink(self.tfile)
        self.timeout = timeout_secs

    def add_san_options(self, san_env, values):
        for options in ['ASAN', 'UBSAN', 'MSAN', 'LSAN']:
            san_env['{}_OPTIONS'.format(options)] = "abort_on_error={}:symbolize={}".format(*values)
        return san_env

    def exec_verify(self, fd, sample):
        run_cmd = shlex.split(self.target_cmd)
        if self.asan_cmd:
            run_cmd = shlex.split(self.asan_cmd)

        asan_env = os.environ.copy()
        asan_env = self.add_san_options(asan_env, (1, 0))

        p = Popen(run_cmd,
                  stdin=fd, stderr=DEVNULL, stdout=DEVNULL,
                  env=asan_env,
                  start_new_session=True)
        try:
            p.wait(timeout=self.timeout)
        except TimeoutExpired:
            self.out_queue.put((sample, 'timeout'))
            p.kill()
            p.wait()
        except:  # noqa
            p.kill()
        v = p.returncode
        # check if process was terminated/stopped by signal
        if not os.WIFSIGNALED(v) and not os.WIFSTOPPED(v):
            self.out_queue.put((sample, 'invalid'))
        if (os.WTERMSIG(v) or os.WSTOPSIG(v)) in [1, 2]:
            self.out_queue.put((sample, 'invalid'))

    def _exec(self, fd, sample):
        run_cmd = shlex.split(self.target_cmd)

        p = Popen(run_cmd,
                  stdin=fd, stderr=DEVNULL, stdout=DEVNULL,
                  preexec_fn=set_mem_limit,
                  start_new_session=True)
        try:
            p.wait(timeout=self.timeout)
        except TimeoutExpired:
            p.kill()
        except:  # noqa
            p.kill()
        return p.returncode

    def exec_asan(self, fd, sample):
        asan_env = os.environ.copy()
        asan_env = self.add_san_options(asan_env, (1, 1))

        p = Popen(shlex.split(self.asan_cmd),
                  stdin=fd,
                  stderr=PIPE,
                  stdout=DEVNULL,
                  env=asan_env,
                  start_new_session=True)
        try:
            sout, serr = p.communicate(timeout=self.timeout)
        except TimeoutExpired:
            p.kill()
            print_err("timeout expired for '{}".format(sample))
            sout, serr = p.communicate()
        except:  # noqa
            p.kill()
            sout, serr = p.communicate()

        asan_log = serr.decode(errors='replace')

        asan_results = parse_asan_log(asan_log)
        self.out_queue.put({'asan': asan_results, 'sample_id': sample})

    def exec_gdb(self, fd, sample):
        asan_env = os.environ.copy()
        asan_env = self.add_san_options(asan_env, (1, 0))

        run_gdb = self.gdb_cmd + [
            "--ex", "exploitable -m -s '{}'".format(sample),
            "--args"] + shlex.split(self.target_cmd)
        p = Popen(run_gdb,
                  stdout=PIPE,
                  stderr=STDOUT,
                  stdin=fd,
                  env=asan_env,
                  start_new_session=True)
        try:
            sout, serr = p.communicate(timeout=self.timeout)
        except (TimeoutExpired, CalledProcessError):
            p.kill()
            print_err("timeout expired for: '{}'".format(sample))
            sout, serr = p.communicate()

        script_output = sout.decode(errors='replace').splitlines()

        for line in script_output:
            if 'classification' in line:
                with suppress(json.JSONDecodeError):
                    j = json.loads(line)
                    self.out_queue.put(j)
            if 'LAVALOG:' in line:
                d = parse_lava_log(line)
                self.out_queue.put({'lava': d, 'sample_id': sample})
            if 'MAGMALOG:' in line:
                d = parse_magma_log(line)
                self.out_queue.put({'lava': d, 'sample_id': sample})

    def run(self):  # noqa C901
        stdin_fd = DEVNULL
        while True:
            f = self.in_queue.get()
            if f is None:
                break

            if isinstance(f, dict):
                tc = f['input']
            else:
                tc = f

            if self.use_stdin:
                stdin_fd = open(tc, 'rb')
            else:
                try:
                    self.link(os.path.abspath(tc), self.tfile)
                except OSError:
                    self.link = os.symlink
                    self.link(os.path.abspath(tc), self.tfile)

            if self.gdb_cmd is not None:
                self.exec_gdb(stdin_fd, f['output'])

            if self.verify:
                self.exec_verify(stdin_fd, tc)
            elif self.asan_cmd is not None:
                self.exec_asan(stdin_fd, f['output'])
            else:
                self._exec(stdin_fd, tc)

            if self.use_stdin:
                stdin_fd.close()
            else:
                os.remove(self.tfile)
            self.in_queue.task_done()


class AflTminThread(threading.Thread):
    def __init__(self, thread_id, tmin_cmd, target_cmd, output_dir, in_queue):
        threading.Thread.__init__(self)
        self.id = thread_id
        self.target_cmd = target_cmd
        self.output_dir = output_dir
        self.in_queue = in_queue
        self.tmin_cmd = tmin_cmd

    def run(self):
        while True:
            f = self.in_queue.get()
            if f is None:
                break

            cmd = "%s -i %s -o %s -- %s" % (self.tmin_cmd, f, os.path.join(self.output_dir, os.path.basename(f)),
                                            self.target_cmd)
            with Popen(cmd, stderr=DEVNULL, stdout=DEVNULL, shell=True, start_new_session=True) as p:
                try:
                    p.wait()
                except Exception as e:
                    print_err("afl-tmin process error: {} ".format(e))
            self.in_queue.task_done()
