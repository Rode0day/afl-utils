"""
Copyright 2019-2021 @DynaWhat <jmb@iseclab.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import argparse
from concurrent.futures import ThreadPoolExecutor
from contextlib import suppress
import json
import os
import re
import resource
import shlex
import shutil
import subprocess
import threading
import time

from afl_utils import afl_collect
from afl_utils import common
from afl_utils.AflPrettyPrint import clr, print_ok, print_warn, print_err  # noqa
from afl_utils.LuckyFuzz import LuckyFuzzClient


def set_mem_limit():
    soft, hard = resource.getrlimit(resource.RLIMIT_AS)
    memlimit = 2**30 * 1  # 1 GB
    resource.setrlimit(resource.RLIMIT_AS, (memlimit, memlimit))

# lcov --zerocounters --directory .
# lcov --rc lcov_branch_coverage=1 --no-checksum --no-external --capture --initial --directory . --output-file base.info  # noqa
# lcov --rc lcov_branch_coverage=1 --no-checksum --no-external --capture --directory . --output-file test.info
# lcov --rc lcov_branch_coverage=1 --add-tracefile base.info --add-tracefile test.info --output-file coverage.info
# genhtml --branch-coverage --ignore-errors source coverage.info --legend --title picotls --output-directory=cc_report


class AflGcov(object):

    def __init__(self, settings, include_crashes=False):
        self.timeout = 5
        # use 'gcov_target' as alt target if it is defined, otherwise use 'gcov/src/<target>'
        gcov_bin = os.path.join('gcov', 'src', os.path.basename(settings['target']))
        if 'gcov_target' in settings:
            gcov_bin = settings['gcov_target']

        # use 'gcov_src_dir' if defined, otherwise the default source dir is 'gcov'
        self.gcov_src_dir = 'gcov'
        if 'gcov_src_dir' in settings:
            self.gcov_src_dir = settings['gcov_src_dir']
        self.gcov_src_dir = settings['gcov_src_dir'] if 'gcov_src_dir' in settings else 'gcov'

        if os.path.exists(gcov_bin) and os.path.exists(self.gcov_src_dir):
            self.target_cmd = common.build_target_cmd(settings, gcov_bin)
        else:
            print_err("Could not find gcov binary and/or source!")
            self.target_cmd = None
            self.q_samples = []
            return

        self.num_workers = int(settings['n_workers'])
        sync_dir = settings['output']
        self.cov_dir = os.path.join(sync_dir, 'cov')
        self.base_info = os.path.join(self.cov_dir, 'gcov', 'base.info')

        fuzzers = afl_collect.get_fuzzer_instances(sync_dir, crash_dirs=False, both=include_crashes)
        q_index = afl_collect.build_sample_index(sync_dir, '/dev/null', fuzzers, omit_fuzzer_name=True)
        self.q_samples = q_index.inputs()
        self.lcov = shutil.which('lcov')
        self.grcov = shutil.which('grcov')
        self.genhtml = shutil.which('genhtml') if settings.get('genhtml') else None
        self.verbose = settings.get('verbose', False)

    def lcov_initialize(self):
        gcov_dir = os.path.dirname(self.base_info)
        os.makedirs(gcov_dir, exist_ok=True)
        args = [self.lcov, '--zerocounters', '--directory', self.gcov_src_dir]
        p = subprocess.run(args, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

        args = [self.lcov, '--rc', 'lcov_branch_coverage=1', '--no-checksum', '--no-external', '--capture',
                '--initial', '--directory', self.gcov_src_dir, '--output-file', self.base_info]
        p = subprocess.run(args, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, timeout=600)
        return p.returncode

    def lcov_final(self):
        self.coverage_info = os.path.join(self.cov_dir, 'gcov', 'coverage.info')
        if self.grcov:
            args = [self.grcov, '--branch', '-o', self.coverage_info, self.gcov_src_dir]
        else:
            args = [self.lcov, '--rc', 'lcov_branch_coverage=1', '--no-checksum', '--no-external', '--capture',
                    '--directory', self.gcov_src_dir, '--output-file', self.coverage_info]
        with suppress(subprocess.TimeoutExpired):
            if self.verbose:
                print_ok("Executing: {}".format(' '.join(args)))
            p = subprocess.run(args, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, timeout=600)

        stats = dict()
        args = [self.lcov, '--rc', 'lcov_branch_coverage=1', '--summary', self.coverage_info]
        with suppress(subprocess.TimeoutExpired):
            if self.verbose:
                print_ok("Executing: {}".format(' '.join(args)))
            p = subprocess.run(args, stderr=subprocess.STDOUT, stdout=subprocess.PIPE,
                               universal_newlines=True, timeout=600)
        if p.returncode != 0:
            return stats

        re_cov = re.compile(r'^\s+(\w+)\.+:\s([\d\.]+)%\s\((\d+)\sof\s(\d+)\s\w+\)')
        for line in p.stdout.splitlines():
            m = re_cov.search(line)
            if m is None:
                continue
            if m.group(1) == 'lines':
                print_ok(line)
                stats['l_ratio'] = float(m.group(2))
                stats['l_hit'] = int(m.group(3))
                stats['l_total'] = int(m.group(4))
            if m.group(1) == 'functions':
                stats['f_ratio'] = float(m.group(2))
                stats['f_hit'] = int(m.group(3))
                stats['f_total'] = int(m.group(4))
            if m.group(1) == 'branches':
                stats['b_ratio'] = float(m.group(2))
                stats['b_hit'] = int(m.group(3))
                stats['b_total'] = int(m.group(4))
        return stats

    def gen_web(self):
        webdir = os.path.join(self.cov_dir, 'web')
        args = [self.genhtml, '--branch-coverage', '--output-directory', webdir, self.coverage_info]
        print_ok("Generating web report in {}".format(webdir))
        if self.verbose:
            print_ok("Executing: {}".format(' '.join(args)))
        p = subprocess.run(args, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, timeout=600)
        return p.returncode

    def run_target(self, cur_input):
        use_stdin = None
        # replace '@@' with sample input or use stdin
        if '@@' in self.target_cmd:
            tid = threading.get_ident()
            tfile = ".dr_input{:06d}".format(tid)
            os.link(cur_input, tfile)
            tgt_cmd = self.target_cmd.replace('@@', tfile)
        else:
            tgt_cmd = self.target_cmd
            use_stdin = open(cur_input, 'r')
        args = shlex.split(tgt_cmd)

        p = subprocess.Popen(args,
                             stdin=use_stdin,
                             stdout=subprocess.DEVNULL,
                             stderr=subprocess.DEVNULL,
                             preexec_fn=set_mem_limit,
                             start_new_session=True)
        try:
            p.wait(timeout=self.timeout)
        except Exception as e:
            if isinstance(e, subprocess.TimeoutExpired):
                print_err("Timeout (%d)s expired for : '%s'" % (self.timeout, cur_input))
                ret = -99
            else:
                print_err("%s \n\tfor: %s" % (str(e), cur_input))
            p.kill()
            p.wait()
        finally:
            ret = p.returncode
            if use_stdin is not None:
                use_stdin.close()
            else:
                os.remove(tfile)
        return ret

    def collect_coverage(self):
        gcov_stats = {'skipped': 0}
        if self.target_cmd is None:
            gcov_stats['skipped'] = len(self.q_samples)
            return gcov_stats
        if len(self.q_samples) == 0:
            print_warn("No valid samples found")
            return gcov_stats

        # initialize with zero coverage counters
        self.lcov_initialize()
        print_ok("Executing cmd: {} with {} threads".format(self.target_cmd, self.num_workers))

        with ThreadPoolExecutor(max_workers=self.num_workers) as pool:
            res = pool.map(self.run_target, self.q_samples)
        gcov_stats['skipped'] = sum([1 for r in res if r == -99])

        # Attempt to run lcov and get line coverage stats
        gcov_stats.update(self.lcov_final())

        with open(os.path.join(self.cov_dir, 'gcov_stats.json'), 'w') as gcov_stats_file:
            json.dump(gcov_stats, gcov_stats_file)

        if self.genhtml:
            self.gen_web()

        return gcov_stats


def main(argv=None):
    parser = argparse.ArgumentParser(description="Collect gcov stats and submit to LuckyFuzz")
    parser.add_argument("-c", "--config", dest="config_file",
                        help="afl-stats config file (Default: job.json)!", default="job.json")
    parser.add_argument('-f', '--force', dest='force', action='store_const', const=True,
                        help='Overwrite existing coverage folder, if it exists.', default=False)
    parser.add_argument("-j", "--processes", dest="num_procs", default=2,
                        help="Enable parallel gcov execution and collection processing.")
    parser.add_argument('-s', '--submit', dest='submit', action='store_const', const=True,
                        help='Submit cov stats to LuckyFuzz. (requires `.luckyfuz` config file', default=False)
    parser.add_argument('-x', '--crashes', dest='inc_crashes', action='store_const', const=True,
                        help='Process crashes in addition to queue inputs.', default=False)
    parser.add_argument('-w', '--web', dest='genhtml', action='store_const', const=True,
                        help='Use genhtml to generate the web report.', default=False)
    parser.add_argument('-v', '--verbose', dest='verbose', action='store_const', const=True,
                        help='Verbose logging enabled.', default=False)
    parser.add_argument('--dir', dest='project_dir',
                        help='Process all directories in <project_dir> as targets.')
    args = parser.parse_args(argv)

    if args.project_dir:
        target_dirs = list()
        for cur_dir in os.scandir(args.project_dir):
            cfile = os.path.join(cur_dir.path, args.config_file)
            if cur_dir.is_dir() and os.path.isfile(cfile):
                target_dirs.append(cur_dir.path)
    else:
        target_dirs = ['.']

    start = time.time()
    for tdir in target_dirs:
        os.chdir(tdir)
        cfile = os.path.join(tdir, args.config_file)

        config_settings = common.read_config(cfile)
        config_settings['n_workers'] = args.num_procs

        config_settings['genhtml'] = args.genhtml
        config_settings['verbose'] = args.verbose

        if not os.path.isdir(os.path.join(tdir, config_settings['output'])):
            continue

        stats_file = os.path.join(tdir, config_settings['output'], 'cov', 'gcov_stats.json')
        if not args.force and os.path.isfile(stats_file):
            continue

        executor = AflGcov(config_settings, args.inc_crashes)
        stats = executor.collect_coverage()

        if args.submit:
            lf_client = LuckyFuzzClient(config_settings)
            if lf_client is not None:
                print(lf_client.submit_cov_stats(stats))
        print_ok("Completed in {:.2f} seconds".format(time.time() - start))


if __name__ == "__main__":
    main()
