"""
Copyright 2019-2021 @DynaWhat <jmb@iseclab.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Forkserver from https://github.com/eqv/aflq_fast_cov
"""

from collections import Counter
import argparse
import csv
import glob
import hashlib
import multiprocessing
import os
import queue
import resource
import shlex
import shutil
import signal
import struct
import sys
import tempfile
import time

from contextlib import contextmanager

from afl_utils import afl_collect
from afl_utils import common
from afl_utils.AflPrettyPrint import clr, print_ok, print_warn, print_err  # noqa
from afl_utils.common import save_json_data
from afl_utils.LuckyFuzz import LuckyFuzzClient

# globals: update via command args
global_config = {
    "no_chain": False,
    "log_each": False,
    "quiet": True
}


def shahexdigest(filepath, hash_name='sha1'):
    m = hashlib.new(hash_name)
    m.update(filepath.encode('utf-8'))
    return m.hexdigest()


def bitness(filepath):
    with open(filepath, "rb") as f:
        data = f.read(32)
    ei_class = data[4]  # 1=32, 2=64
    if ei_class in [1, 2]:
        return ei_class * 32
    e_machine = data[18]
    raise Exception("[!] Unexpected value in elf header: %s (ei_class: %d, e_machine: 0x%x)" %
                    (filepath, e_machine, ei_class))


class TimeoutError(Exception):
    pass


class AflFastCov(object):

    def __init__(self, settings, include_crashes=False, save_cov=False):
        # use 'drcov_target' as alt target if it is defined, otherwise use 'target'
        self.target_cmd = common.build_target_cmd(settings, settings.get('drcov_target'))
        # use afl-fast-cov path based on binary (32/64)
        target_binary = self.target_cmd.split()[0]
        covbin = "afl-fast-cov_{}".format(bitness(target_binary))
        self.fast_cov_path = shutil.which(covbin)
        if self.fast_cov_path is None:
            raise Exception("[!] %s binary not found!", covbin)

        self.num_workers = int(settings['n_workers'])
        self.sync_dir = settings['output']
        self.fcov_dir = os.path.join(self.sync_dir, 'fcov')
        self.save_cov = save_cov
        self.log_cols = ['id', 'bb_cov', 'edge_cov', 'new_bb', 'new_edge', 'all_bb', 'all_edge', 'path']
        self.bb_cov_set = Counter()
        self.edge_cov_set = Counter()

        fuzzers = afl_collect.get_fuzzer_instances(self.sync_dir, crash_dirs=False, both=include_crashes)
        q_index = afl_collect.build_sample_index(self.sync_dir, '/dev/null', fuzzers, omit_fuzzer_name=True)
        self.q_samples = q_index.inputs()

    def collect_coverage(self):  # noqa C901
        fcov_stats = {'cov_bb': 0, 'cov_edge': 0, 'skipped': 0}
        if len(self.q_samples) == 0:
            print_warn("No valid samples found")
            return fcov_stats

        # Ignore SIGALRM in parent process
        signal.signal(signal.SIGALRM, signal.SIG_IGN)

        with tempfile.TemporaryDirectory() as logdir:
            print_ok("Executing cmd: {} {}".format(self.fast_cov_path, self.target_cmd))
            traces = dict()
            len_q = len(self.q_samples)
            print_ok("Collecting coverage with fcov on {} samples.".format(len_q))
            start = time.time()
            sample_queue = multiprocessing.JoinableQueue()
            results_queue = multiprocessing.Queue()
            stop_now = multiprocessing.Event()
            proc_list = []
            for sample in self.q_samples:
                sample_queue.put(sample)
            # Start the worker processes, limit total number based on queue size
            j = min(len_q // 100 + 1, self.num_workers)
            print_ok("Starting {} execution process(s). PID:\n   ".format(j), end=" ")
            for n in range(j):
                p = Forkserver(sample_queue, results_queue, self.target_cmd, self.fast_cov_path, logdir, stop_now)
                proc_list.append(p)
                p.start()
                if sample_queue.qsize() < 100:
                    break

            # wait for tasks to complete
            try:
                sample_queue.join()
            except KeyboardInterrupt:
                print_warn("forced collecting results!")

            # tell workers to quit
            stop_now.set()
            for p in proc_list:
                if p.is_alive():
                    sample_queue.put(None)

            # wait for queues
            try:
                sample_queue.join()
            except KeyboardInterrupt:
                print_warn("forced collecting results!")

            # process the results
            while True:
                try:
                    result = results_queue.get(block=True, timeout=120)
                except queue.Empty:
                    break
                if result is None:
                    continue
                if 'logpath' in result:
                    traces[result['path']] = result
                else:
                    fcov_stats['skipped'] += 1
                if results_queue.empty():
                    break
            sample_queue.close()

            # wait for task completion
            print("", flush=True)
            print_ok("Shutting down execution process.")
            for p in proc_list:
                p.join(timeout=2)
            for p in proc_list:
                if p.is_alive():
                    p.terminate()
            elapsed = time.time() - start
            n_processed = len(traces)
            print_ok("Completed executing samples in {:.2f} seconds; {} traces".format(elapsed, n_processed))

            # create results dir
            if not os.path.isdir(self.fcov_dir):
                os.mkdir(self.fcov_dir)

            # process all trace files for unique coverage
            fcov_stats.update(self.process_traces(traces))
            print_ok("Completed processing samples in {:.2f} seconds".format((time.time() - start)))

            if (self.save_cov):
                cov_tgz = os.path.join(self.fcov_dir, 'fcov_logs')
                shutil.make_archive(cov_tgz, 'gztar', logdir)
        save_json_data(fcov_stats, os.path.join(self.fcov_dir, 'fcov_stats.json'))

        return fcov_stats

    def write_bb_cov_set_file(self):
        bb_cov_file = os.path.join(self.fcov_dir, 'bb_cov.log')
        with open(bb_cov_file, 'w') as f:
            f.write("block,size,count\n")
            writer = csv.writer(f)
            for k, v in self.bb_cov_set.items():
                writer.writerow([k[0], k[1], v])

    def write_edge_cov_set_file(self):
        edge_cov_file = os.path.join(self.fcov_dir, 'edge_cov.log')
        with open(edge_cov_file, 'w') as f:
            f.write("src,dst,count\n")
            writer = csv.writer(f)
            for k, v in self.edge_cov_set.items():
                writer.writerow([k[0], k[1], v])

    def num_blocks(self):
        return len(self.bb_cov_set)

    def num_edges(self):
        return len(self.edge_cov_set)

    def process_traces(self, traces):
        csv_fstr = "{id}\t{bb_cov}\t{edge_cov}\t{new_bb}\t{new_edge}\t{all_bb}\t{all_edge}\t{path}\n"
        csv_file_path = os.path.join(self.fcov_dir, 'fcov_plot_data')
        with open(csv_file_path, 'w') as csvfile:
            print('\t'.join(self.log_cols), file=csvfile)  # noqa
            for tr_path in sorted(traces):
                tr = traces[tr_path]
                tr.update(self.decode_traces(tr['logpath']))
                csvfile.write(csv_fstr.format(**tr))
        self.write_bb_cov_set_file()
        self.write_edge_cov_set_file()
        return {'cov_bb': self.num_blocks(), 'cov_edge': self.num_edges()}

    def decode_traces(self, traces):
        edges = set()
        blocks = set()

        for trace in traces:
            self.decode_trace(trace, blocks, edges)
        new_blocks = len(blocks.difference(set(self.bb_cov_set)))
        new_edges = len(edges.difference(set(self.edge_cov_set)))
        self.bb_cov_set.update(blocks)
        self.edge_cov_set.update(edges)
        return {
            'bb_cov': len(blocks),
            'edge_cov': len(edges),
            'new_bb': new_blocks,
            'new_edge': new_edges,
            'all_bb': self.num_blocks(),
            'all_edge': self.num_edges()
        }

    def decode_trace(self, trfile, blocks, edges):
        with open(trfile, 'r') as fd:
            reader = csv.reader(fd, delimiter=',')
            for row in reader:
                src = int(row[0], 16) if row[0] != 'ffffffffffffffff' else False
                dst = int(row[1], 16) if row[1] != 'ffffffffffffffff' else False
                size = int(row[2], 10)  # noqa
                if dst:
                    blocks.add((dst, size))
                if src and dst:
                    edges.add((src, dst))


class Forkserver(multiprocessing.Process):
    def __init__(self, in_q, out_q, cmd, qemu_path, logdir, shutdown, quiet=True):
        multiprocessing.Process.__init__(self)
        self.ctl_out, self.ctl_in = os.pipe()
        self.st_out, self.st_in = os.pipe()
        self.hide_output = quiet
        self.timeout = 5
        self.timeouts = 0
        self.mem_limit = 2**30 * 4  # mem limit 4GB
        self.in_q = in_q
        self.out_q = out_q
        self.pre_cmd = cmd
        self.qemu_path = qemu_path
        self.trace_logdir = logdir
        self.shutdown_event = shutdown
        self.fork_timeout = False

    def child(self):
        FORKSRV_FD = 198  # from AFL config.h
        os.dup2(self.ctl_out, FORKSRV_FD)
        os.dup2(self.st_in, FORKSRV_FD + 1)

        if self.hide_output:
            null_fd = os.open('/dev/null', os.O_RDWR)
            os.dup2(null_fd, 1)
            os.dup2(null_fd, 2)
            os.close(null_fd)

        os.dup2(self.in_file.fileno(), 0)
        os.close(self.in_file.fileno())

        os.close(self.ctl_in)
        os.close(self.ctl_out)
        os.close(self.st_in)
        os.close(self.st_out)
        os.setsid()
        resource.setrlimit(resource.RLIMIT_CORE, (0, 0))
        resource.setrlimit(resource.RLIMIT_AS, (self.mem_limit, self.mem_limit))
        env = os.environ.copy()
        env["TRACE_OUT_DIR"] = self.trace_logdir
        if global_config["no_chain"]:
            env["QEMU_LOG"] = "nochain"
        if global_config["log_each"]:
            env["AFL_LOG_PER_THREAD"] = "1"
        ARGS = shlex.split(self.cmd)
        os.execve(self.qemu_path, ["afl_fast_cov"] + ARGS, env)
        print("child failed")

    @contextmanager
    def read_timeout(self):

        def handler(signum, sigfr):
            self.fork_timeout = True
            try:
                os.kill(self.fork_pid, signal.SIGKILL)
                os.waitpid(self.fork_pid, 0)
            except (ProcessLookupError, ChildProcessError) as e:
                print(e, flush=True)
            raise TimeoutError(" *killing forkserver {}* ".format(self.fork_pid))

        try:
            signal.signal(signal.SIGALRM, handler)
            signal.setitimer(signal.ITIMER_REAL, 10, 0)
            yield
        except TimeoutError as e:
            print(e, flush=True)
        finally:
            signal.setitimer(signal.ITIMER_REAL, 0, 0)  # disable timer for timeout

    def parent(self):
        os.close(self.ctl_out)
        os.close(self.st_in)
        self.cwd = os.getcwd()
        self.fork_timeout = False
        with self.read_timeout():
            status = os.read(self.st_out, 4)
        if not self.fork_timeout and len(status) == 4:
            return
        self.in_q.close()
        self.out_q.close()
        os.close(self.ctl_in)
        os.close(self.st_out)
        print_err("Forkserver PID {} failed to initialize".format(self.fork_pid))
        sys.exit(-1)

    def run(self):
        self.fork_pid = 0
        # Ignore SIGALRM unless under timer
        signal.signal(signal.SIGALRM, signal.SIG_IGN)
        # Ignore Ctrl-C in this process
        signal.signal(signal.SIGINT, signal.SIG_IGN)

        tfile = ".cov_input{:06d}".format(os.getpid())
        self.in_file = open(tfile, "wb+")
        self.cmd = self.pre_cmd.replace('@@', tfile)

        self.fork_pid = os.fork()
        if self.fork_pid == 0:
            self.child()
            return
        self.parent()

        print("{}, ".format(os.getpid()), end="", flush=True)

        while not self.shutdown_event.is_set():
            # pop one result from the queue
            testcase = self.in_q.get()
            if testcase is None:
                break
            result = self.execute_input(testcase)
            self.in_q.task_done()
            if result is None:
                break
            self.out_q.put(result)
        # processs the main log file if it exists (default mode / log_each turned off)
        result['logpath'] = []
        traces = glob.glob("{}/trace*{:05d}*.qemu".format(self.trace_logdir, self.fork_pid))
        for i, trace in enumerate(traces):
            new_name = os.path.join(self.trace_logdir, "fcov.forksrv-{}-{}.log".format(self.fork_pid, i))
            os.rename(trace, new_name)
            result['logpath'].append(new_name)
            self.out_q.put(result)
        # kill the forkserver if its not already exited
        self.in_q.task_done()
        os.kill(self.fork_pid, signal.SIGKILL)
        os.close(self.ctl_in)
        os.close(self.st_out)
        print("- PID:{} Q:{} timeouts:{}".format(os.getpid(), self.in_q.qsize(), self.timeouts), flush=True)

    def execute_input(self, testcase):

        def handle_timeout(child_pid):
            try:
                os.kill(child_pid, signal.SIGKILL)
            except OSError as e:
                print(e, flush=True)
            self.timeouts += 1

        fname = os.path.basename(testcase)
        fid = int(fname[3:9]) if fname[:3] == 'id:' else 0
        prefix = len(os.path.commonpath([self.cwd, testcase])) + 1
        tc_stat = os.stat(testcase)
        result = {'id': fid,
                  'timestamp': tc_stat.st_mtime,
                  'size': tc_stat.st_size,
                  'path': testcase[prefix:]}

        # write testcase to the input file
        self.in_file.truncate(0)
        self.in_file.seek(0)
        with open(testcase, 'rb') as fd:
            self.in_file.write(fd.read())
        self.in_file.seek(0)

        # execute for cov capture
        if os.write(self.ctl_in, b"\0\0\0\0") != 4:
            print_warn("fork server dead? PID {}".format(os.getpid()))
            return None

        pid = struct.unpack("I", os.read(self.st_out, 4))[0]
        result['pid'] = pid

        signal.signal(signal.SIGALRM, lambda si, fr: handle_timeout(pid))
        signal.setitimer(signal.ITIMER_REAL, self.timeout, 0)
        status = os.read(self.st_out, 4)
        signal.setitimer(signal.ITIMER_REAL, 0, 0)  # disable timer for timeout
        if len(status) != 4:
            print_warn("fork server dead? PID {}".format(os.getpid()))
            return None
        result['status'] = struct.unpack("I", status)[0]

        traces = glob.glob("{}/trace*{:05d}*.qemu".format(self.trace_logdir, pid))
        if not len(traces):
            if global_config["log_each"]:
                print_err("no log created for {}".format(testcase))
            return result

        result['logpath'] = []
        for trace in traces:
            new_name = os.path.join(self.trace_logdir, "fcov.{}.log".format(shahexdigest(testcase)))
            os.rename(trace, new_name)
            result['logpath'].append(new_name)
        return result


def main(argv=None):
    parser = argparse.ArgumentParser(description="Collect coverage stats and submit to LuckyFuzz")
    parser.add_argument("-c", "--config", dest="config_file",
                        help="afl-stats config file (Default: job.json)!", default="job.json")
    parser.add_argument('-f', '--force', dest='force', action='store_const', const=True,
                        help='Overwrite existing coverage folder, if it exists.', default=False)
    parser.add_argument("-j", "--processes", dest="num_procs", default=2,
                        help="Enable parallel cov execution and collection processing.")
    parser.add_argument('-s', '--submit', dest='submit', action='store_const', const=True,
                        help='Submit cov stats to LuckyFuzz. (requires `.luckyfuz` config file', default=False)
    parser.add_argument('-S', '--save', dest='save_cov', action='store_const', const=True,
                        help='Save cov log files as an archive in log dir', default=False)
    parser.add_argument('-x', '--crashes', dest='inc_crashes', action='store_const', const=True,
                        help='Process crashes in addition to queue inputs.', default=False)
    parser.add_argument('--log-each', dest='log_each', action='store_const', const=True,
                        help='Generate log file for each queue input.', default=False)
    parser.add_argument('--no-chain', dest='no_chain', action='store_const', const=True,
                        help='Disable chaining in qemu translation.', default=False)
    parser.add_argument('--dir', dest='project_dir',
                        help='Process all directories in <project_dir> as targets.')
    args = parser.parse_args(argv)

    if args.project_dir:
        target_dirs = list()
        for cur_dir in os.scandir(args.project_dir):
            cfile = os.path.join(cur_dir.path, args.config_file)
            if cur_dir.is_dir() and os.path.isfile(cfile):
                target_dirs.append(cur_dir.path)
    else:
        target_dirs = ['.']

    for tdir in target_dirs:
        os.chdir(tdir)
        cfile = os.path.join(tdir, args.config_file)

        config_settings = common.read_config(cfile)
        config_settings['n_workers'] = args.num_procs

        if not os.path.isdir(os.path.join(tdir, config_settings['output'])):
            continue

        stats_file = os.path.join(tdir, config_settings['output'], 'fcov/fcov_stats.json')
        if not args.force and os.path.isfile(stats_file):
            continue

        global_config["log_each"] = args.log_each
        global_config["no_chain"] = args.no_chain

        executor = AflFastCov(config_settings, args.inc_crashes, args.save_cov)
        stats = executor.collect_coverage()

        print_ok("\tmodule unique blocks : {cov_bb}\n"
                 "\tmodule edge coverage : {cov_edge}".format(**stats))

        if args.submit:
            lf_client = LuckyFuzzClient(config_settings)
            if lf_client is not None:
                print(lf_client.submit_cov_stats(stats))


if __name__ == "__main__":
    main()
