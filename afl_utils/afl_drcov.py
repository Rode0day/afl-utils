"""
Copyright 2019-2021 @DynaWhat <jmb@iseclab.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import argparse
from concurrent.futures import ProcessPoolExecutor
from collections import Counter
import csv
import functools
import glob
import os
import random
import re
import resource
import shlex
import shutil
import string
import subprocess
import tempfile
import time

import afl_utils.drcov as dr
from afl_utils import afl_collect
from afl_utils import common
from afl_utils.AflPrettyPrint import clr, print_ok, print_warn, print_err  # noqa
from afl_utils.common import save_json_data
from afl_utils.LuckyFuzz import LuckyFuzzClient


def bitness(filepath):
    with open(filepath, "rb") as f:
        data = f.read(32)
    ei_class = data[4]  # 1=32, 2=64
    if ei_class in [1, 2]:
        return ei_class * 32
    e_machine = data[18]
    raise Exception("[!] Unexpected value in elf header: %s (ei_class: %d, e_machine: 0x%x)" %
                    (filepath, e_machine, ei_class))


def generate_secret():
    return ''.join(random.choice(string.ascii_letters) for i in range(32))


def set_mem_limit():
    soft, hard = resource.getrlimit(resource.RLIMIT_AS)
    memlimit = 2**30 * 1  # 1 GB
    resource.setrlimit(resource.RLIMIT_AS, (memlimit, memlimit))


class AflDrcov(object):

    def __init__(self, settings, include_crashes=False, save_cov=False):
        # use 'drcov_target' as alt target if it is defined, otherwise use 'target'
        self.target_cmd = common.build_target_cmd(settings, settings.get('drcov_target'))
        self.target_binary = self.target_cmd.split()[0]
        # build drrun and drcov2lcov path based on binary (32/64) and Dynamorio root ('/opt/dr')
        self.path_to_drrun = "%s/bin%d/drrun" % (settings['DR_ROOT'], bitness(self.target_binary))
        self.path_to_dr2lcov = "%s/tools/bin%d/drcov2lcov" % (settings['DR_ROOT'], bitness(self.target_binary))
        if not os.path.exists(self.path_to_drrun):
            raise Exception("[!] drrun binary not found at expected path: %s" % self.path_to_drrun)

        self.num_workers = int(settings['n_workers'])
        self.sync_dir = settings['output']
        self.drcov_dir = os.path.join(self.sync_dir, 'drcov')
        self.save_cov = save_cov
        self.cwd = os.getcwd()
        self.log_cols = ['id', 'bb_all', 'bb_count', 'bb_cov', 'path']
        self.bb_cov_set = Counter()

        fuzzers = afl_collect.get_fuzzer_instances(self.sync_dir, crash_dirs=False, both=include_crashes)
        q_index = afl_collect.build_sample_index(self.sync_dir, '/dev/null', fuzzers, omit_fuzzer_name=True)
        self.q_samples = q_index.inputs()

    def build_drrun_cmd(self, logdir):
        args = [self.path_to_drrun, '-t', 'drcov',
                '-logdir', logdir,
                '--']
        return args

    def write_bb_cov_set_file(self):
        bb_cov_file = os.path.join(self.drcov_dir, 'bb_cov.log')
        with open(bb_cov_file, 'w') as f:
            f.write("block,size,count\n")
            writer = csv.writer(f)
            for k, v in self.bb_cov_set.items():
                writer.writerow([k[0], k[1], v])

    def process_drcov_log(self, path):
        results = dict()
        module = os.path.basename(self.target_binary)
        x = dr.DrcovData(path)
        module_blocks = x.get_offset_blocks(module)
        results['bb_all'] = x.bb_table_count
        results['bb_count'] = len(module_blocks)
        module_block_set = set(module_blocks)
        results['bb_cov'] = len(module_block_set)
        self.bb_cov_set.update(module_block_set)
        return results

    def process_drlogs(self, logs):
        bb_all = tr_len = bb_cov = 0
        csv_file_path = os.path.join(self.drcov_dir, 'drcov_plot_data')
        with open(csv_file_path, 'w') as csvfile:
            print('\t'.join(self.log_cols), file=csvfile)  # noqa
            for tr_path in sorted(logs):
                tr = logs[tr_path]
                tr_id = tr['id']
                tr.update(self.process_drcov_log(tr['logpath']))
                bb_cov = len(self.bb_cov_set)
                bb_all = max(bb_all, tr.get('bb_all'))
                tr_len = max(tr_len, tr.get('bb_count'))
                csvfile.write(
                    "{}\t{}\t{}\t{}\t{}\n".format(tr_id, tr['bb_all'], tr['bb_count'], bb_cov, tr_path))
        self.write_bb_cov_set_file()
        return {'bb_all': bb_all, 'tr_len': tr_len, 'cov_bb': bb_cov}

    def run_lcov(self):
        stats = dict()
        lcov = shutil.which('lcov')
        cov_file = os.path.join(self.drcov_dir, 'dr_coverage.info')
        if lcov is None or not os.path.isfile(cov_file):
            return stats

        args = [lcov, '--summary', cov_file]
        p = subprocess.run(args, stderr=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)
        if p.returncode != 0:
            return stats

        re_cov = re.compile(r'^\s+(\w+)\.+:\s([\d\.]+)%\s\((\d+)\sof\s(\d+)\s\w+\)')
        for line in p.stderr.splitlines():
            m = re_cov.search(line)
            if m and m.group(1) == 'lines':
                print_ok(line)
                stats['l_ratio'] = float(m.group(2))
                stats['l_hit'] = int(m.group(3))
                stats['l_total'] = int(m.group(4))
        return stats

    def run_drcov(self, args, cur_input):
        # locate logdir
        logdir = args[args.index('--') - 1]
        use_stdin = None
        # replace '@@' with sample input or use stdin
        if '@@' in self.target_cmd:
            tfile = ".dr_input{:06d}".format(os.getpid())
            os.link(cur_input, tfile)
            tgt_cmd = self.target_cmd.replace('@@', tfile)
        else:
            tgt_cmd = self.target_cmd
            use_stdin = open(cur_input, 'r')
        args = args + shlex.split(tgt_cmd)
        fname = os.path.basename(cur_input)
        fid = int(fname[3:9]) if fname[:3] == 'id:' else 0
        results = {'id': fid, 'timestamp': os.path.getmtime(cur_input)}

        p = subprocess.Popen(args,
                             stdin=use_stdin,
                             stdout=subprocess.DEVNULL,
                             stderr=subprocess.DEVNULL)
        try:
            p.wait(timeout=25)
        except Exception as e:
            if isinstance(e, subprocess.TimeoutExpired):
                print_err("Timeout (%d)s expired for : '%s'" % (25, cur_input))
            else:
                print_err("%s \n\tfor: %s" % (str(e), cur_input))
            p.kill()
            p.wait()
            return results
        finally:
            if use_stdin is not None:
                use_stdin.close()
            else:
                os.remove(tfile)

        log_files = glob.glob("%s/drcov*%05d*.log" % (logdir, p.pid))

        if len(log_files) == 1 and os.stat(log_files[0]).st_size > 0:
            prefix = len(os.path.commonpath([self.cwd, cur_input])) + 1
            results['path'] = cur_input[prefix:]
            new_name = os.path.join(logdir, "drcov.{}.log".format(generate_secret()))
            os.rename(log_files[0], new_name)
            results['logpath'] = new_name
            return results

        print_warn("Drcov log file not found/corrupt for input {} ({} log files)".format(cur_input, len(log_files)))
        return results

    def collect_coverage(self):
        drcov_stats = {'bb_all': 0, 'tr_len': 0, 'cov_bb': 0, 'skipped': 0}
        if len(self.q_samples) == 0:
            print_warn("No valid samples found")
            return drcov_stats

        with tempfile.TemporaryDirectory() as logdir:
            drrun_cmd = self.build_drrun_cmd(logdir)
            print_ok("Executing drrun cmd: {} {}".format(' '.join(drrun_cmd), self.target_cmd))

            logs = dict()
            module = os.path.basename(self.target_binary)
            len_q = len(self.q_samples)
            print_ok("Collecting coverage with drcov on %d samples." % len_q)
            start = time.time()
            with ProcessPoolExecutor(max_workers=self.num_workers) as pool:
                get_coverage = functools.partial(self.run_drcov, drrun_cmd)
                csize = max(1, len_q // self.num_workers)
                for res in pool.map(get_coverage, self.q_samples, chunksize=csize):
                    if res.get('logpath') is None:
                        drcov_stats['skipped'] += 1
                        continue
                    logs[res['path']] = res

            print_ok("Completed executing samples in {:.2f} seconds".format((time.time() - start)))

            # create results dir
            if not os.path.isdir(self.drcov_dir):
                os.mkdir(self.drcov_dir)

            # process all trace files for unique coverage
            drcov_stats.update(self.process_drlogs(logs))

            # Generate lcov ".info" file, depends on binary having symbols
            cov_file = os.path.join(self.drcov_dir, 'dr_coverage.info')
            args = [self.path_to_dr2lcov, '-dir', logdir, '-output', cov_file, '-mod_filter', module]
            print_ok("Completed processing samples in {:.2f} seconds\n    "
                     "Executing: {}".format((time.time() - start), ' '.join(args)))
            subprocess.run(args, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

            if (self.save_cov):
                cov_tgz = os.path.join(self.drcov_dir, 'drcov_logs')
                shutil.make_archive(cov_tgz, 'gztar', logdir)

        # Attempt to run lcov (if present) and get line coverage stats
        drcov_stats.update(self.run_lcov())

        save_json_data(drcov_stats, os.path.join(self.drcov_dir, 'drcov_stats.json'))

        return drcov_stats


def main(argv=None):
    parser = argparse.ArgumentParser(description="Collect drcov stats and submit to LuckyFuzz")
    parser.add_argument("-c", "--config", dest="config_file",
                        help="afl-stats config file (Default: job.json)!", default="job.json")
    parser.add_argument("-d", "--dr-root", dest="dr_root",
                        help="Path to Dynamorio root (Default: /opt/dr)", default="/opt/dr")
    parser.add_argument('-f', '--force', dest='force', action='store_const', const=True,
                        help='Overwrite existing coverage folder, if it exists.', default=False)
    parser.add_argument("-j", "--processes", dest="num_procs", default=2,
                        help="Enable parallel drcov execution and collection processing.")
    parser.add_argument('-s', '--submit', dest='submit', action='store_const', const=True,
                        help='Submit cov stats to LuckyFuzz. (requires `.luckyfuz` config file', default=False)
    parser.add_argument('-S', '--save', dest='save_cov', action='store_const', const=True,
                        help='Save drcov log files as an archive in log dir', default=False)
    parser.add_argument('-x', '--crashes', dest='inc_crashes', action='store_const', const=True,
                        help='Process crashes in addition to queue inputs.', default=False)
    parser.add_argument('--dir', dest='project_dir',
                        help='Process all directories in <project_dir> as targets.')
    args = parser.parse_args(argv)

    if args.project_dir:
        target_dirs = list()
        for cur_dir in os.scandir(args.project_dir):
            cfile = os.path.join(cur_dir.path, args.config_file)
            if cur_dir.is_dir() and os.path.isfile(cfile):
                target_dirs.append(cur_dir.path)
    else:
        target_dirs = ['.']

    for tdir in target_dirs:
        os.chdir(tdir)
        cfile = os.path.join(tdir, args.config_file)

        config_settings = common.read_config(cfile)
        if 'DR_ROOT' not in config_settings:
            config_settings['DR_ROOT'] = args.dr_root
        config_settings['n_workers'] = args.num_procs

        if not os.path.isdir(os.path.join(tdir, config_settings['output'])):
            continue

        stats_file = os.path.join(tdir, config_settings['output'], 'drcov/drcov_stats.json')
        if not args.force and os.path.isfile(stats_file):
            continue

        executor = AflDrcov(config_settings, args.inc_crashes, args.save_cov)
        stats = executor.collect_coverage()

        print_ok("max module trace block : {tr_len}\n"
                 "\tmodule unique blocks : {cov_bb}\n".format(**stats))

        if args.submit:
            lf_client = LuckyFuzzClient(config_settings)
            if lf_client is not None:
                print(lf_client.submit_cov_stats(stats))


if __name__ == "__main__":
    main()
