"""
Copyright 2015-2021 @_rc0r <hlt99@blinkenshell.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import argparse
import csv
import glob
import os
import sys
import socket
import time
from urllib.error import URLError

import afl_utils
from afl_utils import common
from afl_utils.AflPrettyPrint import clr, print_ok, print_warn, print_err, show_info
from afl_utils.common import load_json_file, save_json_data
from afl_utils.LuckyFuzz import LuckyFuzzClient
from afl_utils.MonitorDatabase import process_monitor_db
from afl_utils import afl_cov
from afl_utils import afl_ccov
from afl_utils import afl_gcov
from afl_utils import afl_drcov
from afl_utils import afl_fast_cov
from afl_utils import sancov
from db_connectors import con_sqlite


valid_fields = {
    "id": "INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT",
    "last_update": "INTEGER NOT NULL",
    "start_time": "INTEGER NOT NULL",
    "fuzzer_pid": "INTEGER NOT NULL",
    "cycles_done": "INTEGER",
    "execs_done": "INTEGER",
    "execs_per_sec": "REAL",
    "paths_total": "INTEGER NOT NULL",
    "paths_favored": "INTEGER",
    "paths_found": "INTEGER NOT NULL",
    "paths_imported": "INTEGER",
    "max_depth": "INTEGER",
    "cur_path": "INTEGER",
    "pending_favs": "INTEGER",
    "pending_total": "INTEGER",
    "variable_paths": "INTEGER",
    "stability": "REAL",
    "bitmap_cvg": "REAL",
    "unique_crashes": "INTEGER NOT NULL",
    "unique_hangs": "INTEGER",
    "last_path": "INTEGER NOT NULL",
    "last_crash": "INTEGER NOT NULL",
    "last_hang": "INTEGER",
    "execs_since_crash": "INTEGER",
    "exec_timeout": "INTEGER",
    "afl_banner": "VARCHAR(200)",
    "afl_version": "VARCHAR(10)",
    "command_line": "VARCHAR(1000)",
    "alive": "INTEGER",
    "runtime": "INTEGER",
    "peak_rss_mb": "INTEGER",
    "edges_found": "INTEGER",
    "slowest_exec_ms": "INTEGER"""
}

db_table_spec = ", ".join(["`{}` {}".format(k, v) for k, v in valid_fields.items()])


def read_config(config_file):
    config_file = os.path.abspath(os.path.expanduser(config_file))
    config = load_json_file(config_file)
    if 'fuzz_dirs' not in config:
        print_err("Invalid config file!")
        sys.exit(2)
    return config


def twitter_init(config):
    try:
        import twitter
        config['twitter_creds_file'] = os.path.abspath(os.path.expanduser(config['twitter_creds_file']))
        if not os.path.exists(config['twitter_creds_file']):
            twitter.oauth_dance("fuzzer_stats", config['twitter_consumer_key'],
                                config['twitter_consumer_secret'], config['twitter_creds_file'])
        oauth_token, oauth_secret = twitter.read_token_file(config['twitter_creds_file'])
        twitter_instance = twitter.Twitter(auth=twitter.OAuth(oauth_token, oauth_secret,
                                                              config['twitter_consumer_key'],
                                                              config['twitter_consumer_secret']))
        return twitter_instance
    except (twitter.TwitterHTTPError, URLError):
        print_err("Network error, twitter login failed! Check your connection!")
        sys.exit(1)


def shorten_tweet(tweet):
    if len(tweet) > 140:
        print_ok("Status too long, will be shortened to 140 chars!")
        short_tweet = tweet[:137] + "..."
    else:
        short_tweet = tweet
    return short_tweet


def fuzzer_alive(pid):
    try:
        os.kill(pid, 0)
    except (OSError, ProcessLookupError):   # noqa for exception
        return 0
    return 1


def parse_stat_file(stat_file):
    if not os.path.isfile(stat_file):
        return None

    with open(stat_file, "r") as f:
        lines = f.readlines()

    stats = dict()

    for line in lines:
        if ' : ' not in line:
            continue
        k, v = map(str.strip, line.split(':', 1))
        if k not in valid_fields:
            continue
        stats[k] = v.strip(" %")
        if k == 'fuzzer_pid':
            stats['alive'] = fuzzer_alive(int(v))
    if 'last_update' not in stats:
        return None
    rt = max(int(stats['last_update']) - int(stats['start_time']), 1)
    stats['runtime'] = rt
    stats['execs_per_sec'] = float(stats['execs_done']) / float(rt)
    stats.pop('target_mode', None)
    if 'peak_rss_mb' in stats and not stats['peak_rss_mb'].isdigit():
        stats.pop('peak_rss_mb', None)
    return stats


def load_stats(fuzzer_dir, client=None):
    fuzzer_dir = os.path.abspath(os.path.expanduser(fuzzer_dir))
    fuzzer_stats = []

    if not os.path.isdir(fuzzer_dir):
        print_warn("Invalid fuzzing directory specified: " + clr.GRA + "%s" % fuzzer_dir + clr.RST)
        return fuzzer_stats

    stat_file = os.path.join(fuzzer_dir, "fuzzer_stats")
    if os.path.isfile(stat_file):
        # single afl-fuzz job
        stats = parse_stat_file(stat_file)
        alive = stats.get('alive') if stats else False
        if client and client.status:
            client.submit_stats(fuzzer_dir, alive=alive)
        if stats:
            return [stats]

    # afl-fuzz with sync-dir
    for fdir in os.listdir(fuzzer_dir):
        stat_file = os.path.join(fuzzer_dir, fdir, "fuzzer_stats")
        if not os.path.isfile(stat_file):
            continue
        stats = parse_stat_file(stat_file)
        alive = stats.get('alive') if stats else False
        if client and client.status:
            client.submit_stats(fdir, alive=alive)
        if stats:
            fuzzer_stats.append(stats)

    return fuzzer_stats


def summarize_stats(stats):
    sum_stat = {
        'fuzzers': len(stats),
        'alive': 0,
        'execs_done': 0,
        'execs_per_sec': 0,
        'paths_total': 0,
        'paths_favored': 0,
        'pending_favs': 0,
        'pending_total': 0,
        'unique_crashes': 0,
        'unique_hangs': 0,
        'afl_banner': 0,
        'total_runtime': 0,
        'host': socket.gethostname()[:10]
    }

    for s in stats:
        for k in sum_stat.keys():
            if k in s.keys():
                if k != "afl_banner":
                    sum_stat[k] += float(s[k])
                else:
                    sum_stat[k] = s[k][:32]
            elif k == 'total_runtime':
                sum_stat[k] = max(sum_stat[k], s['runtime'])

    return sum_stat


def diff_stats(sum_stats, old_stats):
    diff_stat = {
        'fuzzers': 0,
        'alive': 0,
        'execs_done': 0,
        'execs_per_sec': 0,
        'paths_total': 0,
        'paths_favored': 0,
        'pending_favs': 0,
        'pending_total': 0,
        'unique_crashes': 0,
        'unique_hangs': 0,
        'afl_banner': "",
        'total_runtime': 0,
        'host': socket.gethostname()[:10]
    }

    if len(sum_stats) != len(old_stats):
        print_warn("Stats corrupted for '" + clr.GRA + "%s" % sum_stats['afl_banner'] + clr.RST + "'!")
        return diff_stat

    for k in sum_stats.keys():
        if k not in ['afl_banner', 'host']:
            diff_stat[k] = sum_stats[k] - old_stats[k]
        else:
            diff_stat[k] = sum_stats[k]

    return diff_stat


def prettify_stat(stat, dstat, console=True):  # noqa: C901
    _stat = stat.copy()
    _dstat = dstat.copy()
    _stat['execs_done'] /= 1e6
    _dstat['execs_done'] /= 1e6

    if _dstat['alive'] == _dstat['fuzzers'] == 0:
        ds_alive = ""
    else:
        ds_alive = " (%+d/%+d)" % (_dstat['alive'], _dstat['fuzzers'])

    # if int(_dstat['execs_done']) == 0:
    if _dstat['execs_done'] == 0:
        ds_exec = " "
    else:
        ds_exec = " (%+d) " % _dstat['execs_done']

    if _dstat['execs_per_sec'] == 0:
        ds_speed = " "
    else:
        ds_speed = " (%+1.f) " % _dstat['execs_per_sec']

    if _dstat['pending_total'] == _dstat['pending_favs'] == 0:
        ds_pend = ""
    else:
        ds_pend = " (%+d/%+d)" % (_dstat['pending_total'], _dstat['pending_favs'])

    if _dstat['unique_crashes'] == 0:
        ds_crash = ""
    else:
        ds_crash = " (%+d)" % _dstat['unique_crashes']

    if console:
        # colorize stats
        _stat['afl_banner'] = clr.BLU + _stat['afl_banner'] + clr.RST
        _stat['host'] = clr.LBL + _stat['host'] + clr.RST

        lbl = clr.GRA
        if _stat['alive'] == 0:
            alc = clr.LRD
            slc = clr.GRA
        else:
            alc = clr.LGN if _stat['alive'] == _stat['fuzzers'] else clr.YEL
            slc = ""
        clc = clr.MGN if _stat['unique_crashes'] == 0 else clr.LRD
        rst = clr.RST

        # colorize diffs
        if _dstat['alive'] < 0 or _dstat['fuzzers'] < 0:
            ds_alive = clr.RED + ds_alive + clr.RST
        else:
            ds_alive = clr.GRN + ds_alive + clr.RST

        # if int(_dstat['execs_done']) < 0:
        if _dstat['execs_done'] < 0:
            ds_exec = clr.RED + ds_exec + clr.RST
        else:
            ds_exec = clr.GRN + ds_exec + clr.RST

        if _dstat['execs_per_sec'] < 0:
            ds_speed = clr.RED + ds_speed + clr.RST
        else:
            ds_speed = clr.GRN + ds_speed + clr.RST

        if _dstat['unique_crashes'] < 0:
            ds_crash = clr.RED + ds_crash + clr.RST
        else:
            ds_crash = clr.GRN + ds_crash + clr.RST

        ds_pend = clr.GRA + ds_pend + clr.RST

        pretty_stat = (
            "[%s on %s]\n %sAlive:%s   %s%d/%d%s%s\n %sExecs:%s   %d%sm\n %sSpeed:%s   %s%.1f%sx/s%s\n"
            " %sPend:%s    %d/%d%s\n %sCrashes:%s %s%d%s%s") %\
            (_stat['afl_banner'], _stat['host'], lbl, rst, alc, _stat['alive'],
             _stat['fuzzers'], rst, ds_alive, lbl, rst, _stat['execs_done'], ds_exec, lbl, rst, slc,
             _stat['execs_per_sec'], ds_speed, rst, lbl, rst, _stat['pending_total'],
             _stat['pending_favs'], ds_pend, lbl, rst, clc, _stat['unique_crashes'], rst, ds_crash)
    else:
        pretty_stat = "[%s #%s]\nAlive: %d/%d%s\nExecs: %d%sm\nSpeed: %.1f%sx/s\n" \
                      "Pend: %d/%d%s\nCrashes: %d%s" %\
                      (_stat['afl_banner'], _stat['host'], _stat['alive'], _stat['fuzzers'], ds_alive,
                       _stat['execs_done'], ds_exec, _stat['execs_per_sec'], ds_speed,
                       _stat['pending_total'], _stat['pending_favs'], ds_pend, _stat['unique_crashes'], ds_crash)
    return pretty_stat


def collect_coverage(config_settings, sync_dir):
    gcov_dir = 'gcov'
    if 'gcov_src_dir' in config_settings:
        gcov_dir = config_settings['gcov_src_dir']

    gcov_bin = os.path.join('gcov', 'src', os.path.basename(config_settings['target']))
    if 'gcov_target' in config_settings:
        gcov_bin = config_settings['gcov_target']

    if not os.path.isfile(gcov_bin):
        print_err("gcov binary ({}) not found".format(gcov_bin))
        return {}

    target_cmd = common.build_target_cmd(config_settings, gcov_bin)
    args = ['--coverage-cmd', target_cmd.replace('@@', 'AFL_FILE'),
            '--afl-fuzzing-dir', sync_dir,
            '--code-dir', gcov_dir,
            '--overwrite',
            '--enable-branch-coverage',
            '--quiet']
    args.extend(config_settings['afl_cov_args'])
    print_ok("Executing afl-cov: {}".format(' '.join(args)))
    cargs = afl_cov.parse_cmdline(args)

    stats = afl_cov.process_afl_test_cases(cargs)

    save_json_data(stats, os.path.join(sync_dir, 'cov', 'gcov_stats.json'))

    return stats


def dump_stats(config_settings, database):
    for sync_dir in config_settings['fuzz_dirs']:
        fuzzer_stats = load_stats(sync_dir)
        for fuzzer in fuzzer_stats:
            # create different table for every afl instance
            # table = 'fuzzer_stats_{}'.format(fuzzer['afl_banner'])
            #
            # django compatible: put everything into one table (according
            # to django plots app model)
            # Differentiate data based on afl_banner, so don't override
            # it manually! afl-multicore will create a unique banner for
            # every fuzzer!
            table = 'aflutils_fuzzerstats'
            if database.init_database(db_table_spec, table) and \
               not database.dataset_exists(fuzzer, ['last_update', 'afl_banner'], table):
                database.insert_dataset(fuzzer, table)


def fetch_stats(config_settings, twitter_inst, submit=False):
    stat_dict = dict()
    headers = ['host', 'afl_banner', 'total_runtime', 'execs_done', 'execs_per_sec',
               'paths_total', 'unique_crashes', 'unique_hangs', 'paths_favored',
               'pending_favs', 'pending_total', 'alive', 'cov_bb', 'cov_edge',
               'l_hit', 'l_total', 'f_hit', 'b_hit', 'cov_pc', 'crashes_out',
               'major_remain', 'asan_remain', 'asan_s_remain', 'fuzzers']

    if config_settings.get('dump_raw', False):
        writer = csv.DictWriter(sys.stdout, headers, extrasaction='ignore')
        writer.writeheader()

    lf_client = LuckyFuzzClient(config_settings) if submit else None

    for fuzzer in config_settings['fuzz_dirs']:
        stats = load_stats(fuzzer, client=lf_client)

        if len(stats) == 0:
            continue

        sum_stats = summarize_stats(stats)

        cov_stats = {'cov_bb': 0, 'cov_edge': 0, 'l_hit': 0, 'l_total': 0, 'crashes_out': 0,
                     'b_hit': 0, 'major_remain': 0, 'asan_remain': 0, 'asan_s_remain': 0}
        gcov_stats_filepath = os.path.join(fuzzer, 'cov', 'gcov_stats.json')
        ccov_stats_filepath = os.path.join(fuzzer, 'cov', 'ccov_stats.json')
        drcov_stats_filepath = os.path.join(fuzzer, 'drcov', 'drcov_stats.json')
        fcov_stats_filepath = os.path.join(fuzzer, 'fcov', 'fcov_stats.json')
        crash_stats_filepath = os.path.join(fuzzer, 'crash_stats.json')
        monitor_stats_filepath = os.path.join(fuzzer, 'monitor_stats.json')
        mon_db_filepath = os.path.join(fuzzer, 'monitor_cov.db')
        sancov_dir = os.path.join(fuzzer, 'sancov')
        sancov_stats_filepath = os.path.join(fuzzer, 'sancov', 'sancov_stats.json')

        # run afl-gcov (gcov) or check for previous run
        if config_settings.get('afl_gcov', False):
            start = time.time()
            gcov = afl_gcov.AflGcov(config_settings, include_crashes=True)
            new_stats = gcov.collect_coverage()
            if isinstance(new_stats, dict):
                cov_stats.update(new_stats)
            print_ok("Completed afl-gcov stats in {:.2f} seconds".format(time.time() - start))
        else:
            load_json_file(gcov_stats_filepath, data=cov_stats, fail=False)

        # run afl-ccov (Clang source based code coverage) or check for previous results
        if config_settings.get('afl_ccov', False):
            start = time.time()
            ccov = afl_ccov.AflCcov(config_settings, include_crashes=True)
            new_stats = ccov.collect_coverage()
            if isinstance(new_stats, dict):
                cov_stats['ccov'] = new_stats
                # This clobbers gcov stats
                cov_stats.update(new_stats)
            print_ok("Completed afl-ccov stats in {:.2f} seconds".format(time.time() - start))
        else:
            load_json_file(ccov_stats_filepath, data=cov_stats, fail=False)

        # run afl-drcov or check for previous run
        if config_settings.get('afl_drcov', False):
            drcov = afl_drcov.AflDrcov(config_settings, include_crashes=True)
            cov_stats.update(drcov.collect_coverage())
        else:
            load_json_file(drcov_stats_filepath, data=cov_stats, fail=False)

        # run afl-fast-cov (qemu) or check for previous run
        if config_settings.get('fast_cov', False):
            fcov = afl_fast_cov.AflFastCov(config_settings, include_crashes=True)
            cov_stats.update(fcov.collect_coverage())
        else:
            load_json_file(fcov_stats_filepath, data=cov_stats, fail=False)

        # collect sancov covered PCs
        if os.path.isfile(sancov_stats_filepath):
            load_json_file(sancov_stats_filepath, data=cov_stats)
        elif os.path.isdir(sancov_dir):
            file_list = glob.glob(sancov_dir + "/*.sancov")
            if len(file_list) > 0:
                s = sancov.Merge(file_list)
                cov_stats['cov_pc'] = len(s)

        # check for crash stats file
        load_json_file(crash_stats_filepath, data=cov_stats, fail=False)

        # check for monitor stats json file
        load_json_file(monitor_stats_filepath, data=cov_stats, fail=False)
        if config_settings.get('mon_plot', False) and os.path.isfile(mon_db_filepath):
            mon_plot_filepath = os.path.join(fuzzer, 'monitor_plot')
            process_monitor_db(mon_db_filepath, mon_plot_filepath)

        if lf_client is not None and lf_client.status:
            lf_client.submit_job_stats(sum_stats, cov_stats)

        old_stats = load_json_file('.afl_stats.{}'.format(os.path.basename(fuzzer)), fail=False)
        if len(old_stats) == 0:
            old_stats = sum_stats.copy()

        # initialize/update stat_dict
        stat_dict[fuzzer] = (sum_stats, old_stats)

        stat_change = diff_stats(sum_stats, old_stats)

        save_json_data(sum_stats, '.afl_stats.{}'.format(os.path.basename(fuzzer)))

        if config_settings.get('dump_raw', False):
            cov_stats.update(sum_stats)
            writer.writerow(cov_stats)
        else:
            print(prettify_stat(sum_stats, stat_change, True))

        tweet = prettify_stat(sum_stats, stat_change, False)

        t_len = len(tweet)  # noqa
        c = clr.LRD if t_len > 140 else clr.LGN

        if twitter_inst:
            print_ok("Tweeting status (%s%d" % (c, t_len) + clr.RST + " chars)...")
            try:
                twitter_inst.statuses.update(status=shorten_tweet(tweet))
            except Exception as e:
                print_err("Sending tweet failed (Reason: " + clr.GRA + "%s" % e.__cause__ + clr.RST + ")")


def main(argv=None):
    parser = argparse.ArgumentParser(description="Post selected contents of fuzzer_stats to LuckyFuzz / Twitter.")

    parser.add_argument("-c", "--config", dest="config_file",
                        help="afl-stats config file (Default: afl-stats.conf)!", default="afl-stats.conf")
    parser.add_argument("-d", "--database", dest="database_file",
                        help="Dump stats history into database.")
    parser.add_argument('-t', '--twitter', dest='twitter', action='store_const', const=True,
                        help='Post stats to twitter (Default: off).', default=False)
    parser.add_argument('-r', '--raw', dest='raw', action='store_const', const=True,
                        help='Dump raw summary stats/ not "pretty"  (Default: off).', default=False)
    parser.add_argument('-s', '--submit', dest='submit', action='store_const', const=True,
                        help='Submit stats to LuckyFuzz. (requires `.luckyfuz` config file', default=False)
    parser.add_argument('-q', '--quiet', dest='quiet', action='store_const', const=True,
                        help='Suppress any output (Default: off).', default=False)
    parser.add_argument('-F', '--fast-cov', dest='fast_cov', action='store_const', const=True,
                        help='Run afl-fast-cov to collect block coverage information',
                        default=False)
    parser.add_argument('-g', '--afl-gcov', dest='afl_gcov', action='store_const', const=True,
                        help='Run afl-gcov to collect coverage information (gcov source dir required)',
                        default=False)
    parser.add_argument('-C', '--afl-ccov', dest='afl_ccov', action='store_const', const=True,
                        help='Run afl-ccov to collect Clang coverage information (ccov source dir required)',
                        default=False)
    parser.add_argument('-D', '--afl-drcov', dest='afl_drcov', action='store_const', const=True,
                        help='Run afl-drcov to collect block coverage information (Dynamorio required)',
                        default=False)
    parser.add_argument("-j", "--processes", dest="num_procs", default=2,
                        help="Enable parallel cov execution and collection processing.")
    parser.add_argument('-M', '--monitor-plot', dest='mon_plot', action='store_const', const=True,
                        help='Generate monitor_plot from monitor sqlite db (monitor_cov.db)',
                        default=False)

    args = parser.parse_args(argv)

    if not args.quiet and not args.raw:
        show_info("afl-stats", afl_utils.__author__, "Send stats of afl-fuzz jobs to Twitter.")

    if args.database_file:
        db_file = os.path.abspath(os.path.expanduser(args.database_file))
        lite_db = con_sqlite.sqliteConnector(db_file, verbose=False)
    else:
        lite_db = None

    config_settings = read_config(args.config_file)
    config_settings['afl_gcov'] = args.afl_gcov
    config_settings['afl_ccov'] = args.afl_ccov
    config_settings['dump_raw'] = args.raw
    config_settings['afl_drcov'] = args.afl_drcov
    config_settings['fast_cov'] = args.fast_cov
    config_settings['n_workers'] = args.num_procs
    config_settings['mon_plot'] = args.mon_plot
    if 'DR_ROOT' not in config_settings:
        config_settings['DR_ROOT'] = '/opt/dr'

    if lite_db:
        dump_stats(config_settings, lite_db)
        lite_db.commit_close()

    if args.twitter:
        twitter_inst = twitter_init(config_settings)
    else:
        twitter_inst = None

    fetch_stats(config_settings, twitter_inst, args.submit)


if __name__ == "__main__":
    main(sys.argv)
