"""
Copyright 2019-2021 @DynaWhat <jmb@iseclab.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import argparse
from concurrent.futures import ThreadPoolExecutor
from contextlib import suppress
import json
import os
import resource
import shlex
import shutil
import subprocess
import tempfile
import threading
import time

from afl_utils import afl_collect
from afl_utils import common
from afl_utils.AflPrettyPrint import clr, print_ok, print_warn, print_err  # noqa
from afl_utils.LuckyFuzz import LuckyFuzzClient

# Memory limit for libfuzzer coverage
RSS_LIMIT_MB = 2048

# Per-input execution timeout
UNIT_TIMEOUT = 10

# Max time for libfuzzer coverage
MAX_TOTAL_TIME = 60 * 20


def set_mem_limit():
    soft, hard = resource.getrlimit(resource.RLIMIT_AS)
    memlimit = 2**30 * 1  # 1 GB
    resource.setrlimit(resource.RLIMIT_AS, (memlimit, memlimit))


def llvm_bindir():
    llvm_config = os.environ.get('LLVM_CONFIG')
    args = [llvm_config, '--bindir']
    p = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
    return p.stdout.decode().strip()


class AflCcov:

    def __init__(self, settings, include_crashes=False, save_cov=False):
        self.timeout = 5
        self.save_cov = save_cov
        # use 'ccov_target' as alt target if it is defined, otherwise use 'ccov/src/<target>'
        ccov_bin = os.path.join('ccov', 'src', os.path.basename(settings['target']))
        if 'ccov_target' in settings:
            ccov_bin = settings['ccov_target']

        # check for FuzzBench coverage binary
        self.fuzzbench_coverage_binary = 'fuzzbench_target' in settings
        if self.fuzzbench_coverage_binary:
            ccov_bin = settings['fuzzbench_target']

        if os.path.exists(ccov_bin):
            self.target_cmd = common.build_target_cmd(settings, ccov_bin)
        else:
            print_err("Could not find ccov binary and/or source!")
            self.target_cmd = None
            self.q_samples = []
            return

        self.ccov_bin = ccov_bin
        self.logdir = "/tmp"
        self.num_workers = int(settings['n_workers'])
        sync_dir = settings['output']
        self.cov_dir = os.path.join(sync_dir, 'cov')

        fuzzers = afl_collect.get_fuzzer_instances(sync_dir, crash_dirs=False, both=include_crashes)
        self.queues = [os.path.join(sync_dir, f[0], 'queue') for f in fuzzers if 'queue' in f[1]]
        q_index = afl_collect.build_sample_index(sync_dir, '/dev/null', fuzzers, omit_fuzzer_name=True)
        self.q_samples = q_index.inputs()
        if 'LLVM_CONFIG' in os.environ:
            bindir = llvm_bindir()
            self.llvm_profdata = os.path.join(bindir, 'llvm-profdata')
            self.llvm_cov = os.path.join(bindir, 'llvm-cov')
        else:
            self.llvm_profdata = shutil.which('llvm-profdata')
            self.llvm_cov = shutil.which('llvm-cov')
        self.genhtml = shutil.which('genhtml') if settings.get('genhtml') else None

    def gen_web(self):
        webdir = os.path.join(self.cov_dir, 'web')
        args = [self.genhtml, '--branch-coverage', '--output-directory', webdir, self.coverage_info]
        print_ok("Generating web report in {}".format(webdir))
        p = subprocess.run(args, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, timeout=600)
        return p.returncode

    def llvm_cov_html(self):
        webdir = os.path.join(self.cov_dir, 'web')
        args = [self.llvm_cov, 'show', '--format=html',
                '-instr-profile={}'.format(self.profdata),
                '-output-dir={}'.format(webdir),
                self.ccov_bin]
        p = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL,
                           universal_newlines=True, timeout=600)
        return p.returncode

    def llvm_profdata_merge(self):
        self.profdata = os.path.join(self.cov_dir, 'ccov', 'llvm.profdata')
        args = [self.llvm_profdata, 'merge', '-sparse', '-o', self.profdata, "{}/*".format(self.logdir)]

        p = subprocess.run(' '.join(args), stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, shell=True)
        return p.returncode

    def llvm_cov_export(self):
        self.coverage_info = os.path.join(self.cov_dir, 'ccov', 'coverage.info')
        args = [self.llvm_cov, 'export', '--summary-only',
                '--format=lcov',
                '-instr-profile={}'.format(self.profdata),
                self.ccov_bin]
        with suppress(subprocess.TimeoutExpired):
            p = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL,
                               universal_newlines=True, timeout=600)
        with open(self.coverage_info, 'w') as fp:
            fp.write(p.stdout)

        args = [self.llvm_cov, 'export', '--summary-only',
                '-instr-profile={}'.format(self.profdata),
                self.ccov_bin]
        with suppress(subprocess.TimeoutExpired):
            p = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL,
                               universal_newlines=True, timeout=600)
        j = json.loads(p.stdout)

        # save coverage summary data
        cov_summary = os.path.join(self.cov_dir, 'ccov', 'coverage.json')
        with open(cov_summary, 'w') as fp:
            json.dump(j, fp)

        stats = dict()
        if 'data' in j and len(j['data']):
            d = j['data'][0]['totals']
            print_ok(d['lines'])
            stats['l_hit'] = d['lines']['covered']
            stats['l_total'] = d['lines']['count']
            stats['l_ratio'] = d['lines']['percent']

            stats['f_hit'] = d['functions']['covered']
            stats['f_total'] = d['functions']['count']
            stats['f_ratio'] = d['functions']['percent']

            stats['b_hit'] = d['regions']['covered']
            stats['b_total'] = d['regions']['count']
            stats['b_ratio'] = d['regions']['percent']
        return stats

    def run_target(self, cur_input):
        use_stdin = None
        # replace '@@' with sample input or use stdin
        if '@@' in self.target_cmd:
            tid = threading.get_ident()
            tfile = ".dr_input{:06d}".format(tid)
            os.link(cur_input, tfile)
            tgt_cmd = self.target_cmd.replace('@@', tfile)
        else:
            tgt_cmd = self.target_cmd
            use_stdin = open(cur_input, 'r')
        args = shlex.split(tgt_cmd)

        # Environment variables
        env = os.environ.copy()
        env['LLVM_PROFILE_FILE'] = os.path.join(self.logdir, "llvm.%8m.profraw")

        p = subprocess.Popen(args,
                             stdin=use_stdin,
                             stdout=subprocess.DEVNULL,
                             stderr=subprocess.DEVNULL,
                             preexec_fn=set_mem_limit,
                             env=env,
                             start_new_session=True)
        try:
            p.wait(timeout=self.timeout)
        except Exception as e:
            if isinstance(e, subprocess.TimeoutExpired):
                print_err("Timeout (%d)s expired for : '%s'" % (self.timeout, cur_input))
                ret = -99
            else:
                print_err("%s \n\tfor: %s" % (str(e), cur_input))
            p.kill()
            p.wait()
        finally:
            ret = p.returncode
            if use_stdin is not None:
                use_stdin.close()
            else:
                os.remove(tfile)
        return ret

    def execute_fuzzbench_coverage_binary(self):
        env = os.environ.copy()
        env['LLVM_PROFILE_FILE'] = os.path.join(self.logdir, "llvm.%8m.profraw")

        sync_dir = os.path.dirname(self.cov_dir)
        crash_dir = os.path.join(sync_dir, 'crashes')
        merge_dir = self.logdir
        cov_binary = shlex.split(self.target_cmd)[0]
        args = [cov_binary, '-merge=1', '-dump_coverage=1',
                '-artifact_prefix={}/'.format(crash_dir),
                '-timeout={}'.format(UNIT_TIMEOUT),
                '-rss_limit_mb={}'.format(RSS_LIMIT_MB),
                merge_dir]

        print_ok("Executing FuzzBench binary: {} ".format(self.target_cmd))
        for queue_dir in self.queues:
            p = subprocess.Popen(args + [queue_dir],
                                 stdout=subprocess.DEVNULL,
                                 stderr=subprocess.DEVNULL,
                                 env=env,
                                 start_new_session=True)
            try:
                p.wait(timeout=MAX_TOTAL_TIME)
            except Exception as e:
                print_err("%s " % str(e))
            p.kill()
            p.wait()

    def execute_with_threading(self):
        print_ok("Executing cmd: {} with {} threads".format(self.target_cmd, self.num_workers))

        with ThreadPoolExecutor(max_workers=self.num_workers) as pool:
            res = pool.map(self.run_target, self.q_samples)
        return sum([1 for r in res if r == -99])

    def collect_coverage(self):
        ccov_stats = {'skipped': 0}
        if self.target_cmd is None:
            ccov_stats['skipped'] = len(self.q_samples)
            return ccov_stats
        if len(self.q_samples) == 0:
            print_warn("No valid samples found")
            return ccov_stats

        ccov_dir = os.path.join(self.cov_dir, 'ccov')
        os.makedirs(ccov_dir, exist_ok=True)
        with tempfile.TemporaryDirectory() as logdir:
            self.logdir = logdir

            if self.fuzzbench_coverage_binary:
                self.execute_fuzzbench_coverage_binary()
            else:
                ccov_stats['skipped'] = self.execute_with_threading()

            # Merge/index raw profiles
            self.llvm_profdata_merge()
            # Generate line coverage stats
            ccov_stats.update(self.llvm_cov_export())

            if (self.save_cov):
                cov_tgz = os.path.join(self.cov_dir, 'ccov', 'cov_profiles')
                shutil.make_archive(cov_tgz, 'gztar', logdir)

        with open(os.path.join(self.cov_dir, 'ccov_stats.json'), 'w') as ccov_stats_file:
            json.dump(ccov_stats, ccov_stats_file)

        if self.genhtml:
            self.gen_web()

        return ccov_stats


def main(argv=None):
    parser = argparse.ArgumentParser(
        description="Collect Clang source based code coverage (ccov) stats and submit to LuckyFuzz")
    parser.add_argument("-c", "--config", dest="config_file",
                        help="afl-stats config file (Default: job.json)!", default="job.json")
    parser.add_argument('-f', '--force', dest='force', action='store_const', const=True,
                        help='Overwrite existing coverage folder, if it exists.', default=False)
    parser.add_argument("-j", "--processes", dest="num_procs", default=2,
                        help="Enable parallel ccov execution and collection processing.")
    parser.add_argument('-s', '--submit', dest='submit', action='store_const', const=True,
                        help='Submit cov stats to LuckyFuzz. (requires `.luckyfuz` config file', default=False)
    parser.add_argument('-x', '--crashes', dest='inc_crashes', action='store_const', const=True,
                        help='Process crashes in addition to queue inputs.', default=False)
    parser.add_argument('-w', '--web', dest='genhtml', action='store_const', const=True,
                        help='Use genhtml to generate the web report.', default=False)
    parser.add_argument('--dir', dest='project_dir',
                        help='Process all directories in <project_dir> as targets.')
    args = parser.parse_args(argv)

    if args.project_dir:
        target_dirs = list()
        for cur_dir in os.scandir(args.project_dir):
            cfile = os.path.join(cur_dir.path, args.config_file)
            if cur_dir.is_dir() and os.path.isfile(cfile):
                target_dirs.append(cur_dir.path)
    else:
        target_dirs = ['.']

    start = time.time()
    for tdir in target_dirs:
        os.chdir(tdir)
        cfile = os.path.join(tdir, args.config_file)

        config_settings = common.read_config(cfile)
        config_settings['n_workers'] = args.num_procs

        config_settings['genhtml'] = args.genhtml

        if not os.path.isdir(os.path.join(tdir, config_settings['output'])):
            continue

        stats_file = os.path.join(tdir, config_settings['output'], 'cov', 'ccov_stats.json')
        if not args.force and os.path.isfile(stats_file):
            continue

        executor = AflCcov(config_settings, args.inc_crashes)
        stats = executor.collect_coverage()

        if args.submit:
            lf_client = LuckyFuzzClient(config_settings)
            if lf_client is not None:
                print(lf_client.submit_cov_stats(stats))
        print_ok("Completed in {:.2f} seconds".format(time.time() - start))


if __name__ == "__main__":
    main()
