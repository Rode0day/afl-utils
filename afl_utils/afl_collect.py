"""
Copyright 2015-2021 @_rc0r <hlt99@blinkenshell.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import argparse
import exploitable
import os
import queue
import shutil
import sys
from collections import namedtuple

import afl_utils
from afl_utils import SampleIndex, AflThread, afl_multicore
from afl_utils.AflPrettyPrint import clr, print_ok, print_err, print_warn, show_info
from afl_utils.common import save_json_data
from afl_utils.LuckyFuzz import LuckyFuzzClient
from db_connectors import con_sqlite
import datetime

# afl-collect global settings
global_crash_subdirs = "crashes"
global_queue_subdirs = "queue"
global_exclude_files = [
    "README.txt",
    "gdb_script",
    "c.db"
]

fuzzer_stats_filename = "fuzzer_stats"


# afl-collect database table spec
db_table_spec = """
`Sample` TEXT PRIMARY KEY NOT NULL,
`Fuzzer` TEXT,
`Classification` TEXT,
`Classification_Description` TEXT,
`Hash` TEXT,
`MajorHash` TEXT,
`FaultFrame` TEXT,
`Timestamp` DATETIME,
`User_Comment` TEXT,
`AsanHash` TEXT,
`AsanSummary` TEXT,
`LavaBug` TEXT,
`StackDepth` INTEGER
"""

new_columns = [("StackDepth", "INTEGER")]

Sample = namedtuple('Sample', [
    'sample',
    'fuzzer',
    'classification',
    'classification_description',
    'hash',
    'major_hash',
    'fault_frame',
    'timestamp',
    'user_comment',
    'asan_hash',
    'asan_summary',
    'lava_bug',
    'stack_depth'])


def check_gdb():
    if shutil.which("gdb") is None:
        print_err("gdb binary not found!")
        sys.exit(1)


def get_fuzzer_instances(sync_dir, crash_dirs=True, both=False):
    if not os.path.isabs(sync_dir):
        sync_dir = os.path.abspath(sync_dir)

    fuzzer_inst = []
    fuzzer_stats_file = os.path.join(sync_dir, fuzzer_stats_filename)
    if os.path.exists(fuzzer_stats_file) and os.path.isfile(fuzzer_stats_file):
        # we're inside a single instance output dir
        fuzzer_inst.append((sync_dir, []))
    else:
        # we're inside a multi instance sync dir
        for fdir in os.listdir(sync_dir):
            fuzzer_stats_file = os.path.join(sync_dir, fdir, fuzzer_stats_filename)
            if os.path.isdir(os.path.join(sync_dir, fdir)) and os.path.isfile(fuzzer_stats_file):
                fuzzer_inst.append((fdir, []))

    if crash_dirs:
        fuzzer_inst = get_directories(sync_dir, fuzzer_inst, global_crash_subdirs)
    elif both:
        fuzzer_inst = get_directories(sync_dir, fuzzer_inst, global_crash_subdirs)
        fuzzer_inst = get_directories(sync_dir, fuzzer_inst, global_queue_subdirs)
    else:
        fuzzer_inst = get_directories(sync_dir, fuzzer_inst, global_queue_subdirs)

    return fuzzer_inst


def get_directories(sync_dir, fuzzer_instances, subdir_filter):
    for fi in fuzzer_instances:
        fuzz_dir = os.path.join(sync_dir, fi[0])
        for cdir in os.listdir(fuzz_dir):
            if os.path.isdir(os.path.join(fuzz_dir, cdir)) and subdir_filter in cdir:
                fi[1].append(cdir)

    return fuzzer_instances


def get_samples_from_dir(sample_subdir, abs_path=False):
    samples = []
    num_samples = 0
    for f in os.listdir(sample_subdir):
        if os.path.isfile(os.path.join(sample_subdir, f)) and f not in global_exclude_files:
            if abs_path:
                samples.append(os.path.join(sample_subdir, f))
            else:
                samples.append(f)
            num_samples += 1
    return num_samples, samples


def collect_samples(sync_dir, fuzzer_instances):
    samples = []
    num_samples = 0
    for fi in fuzzer_instances:
        fuzz_dir = os.path.join(sync_dir, fi[0])
        fuzz_samples = []

        for cd in fi[1]:
            tmp_num_samples, tmp_samples = get_samples_from_dir(os.path.join(fuzz_dir, cd))
            fuzz_samples.append((cd, sorted(tmp_samples)))
            num_samples += tmp_num_samples

        samples.append((fi[0], fuzz_samples))

    return num_samples, samples


def build_sample_index(sync_dir, out_dir, fuzzer_instances, db=None, min_filename=False, omit_fuzzer_name=False):
    sample_num, samples = collect_samples(sync_dir, fuzzer_instances)
    print_ok("Found %d samples, creating index." % sample_num)

    sample_index = SampleIndex.SampleIndex(out_dir, min_filename=min_filename, omit_fuzzer_name=omit_fuzzer_name)
    print_ok("Index created, processing sync dirs: ", end="")

    for fuzzer in samples:
        print("%s" % fuzzer[0], end="", flush=True)  # noqa
        for sample_dir in fuzzer[1]:
            for sample in sample_dir[1]:
                sample_file = os.path.join(sync_dir, fuzzer[0], sample_dir[0], sample)
                sample_name = sample_index.__generate_output__(fuzzer[0], sample_file)

                if not db or not db.dataset_exists({'Sample': sample_name}, ['Sample']):
                    sample_index.add(fuzzer[0], sample_file)
        print(", ", end="", flush=True)
    print()
    return sample_index


def copy_samples(sample_index):
    files_collected = []
    for sample in sample_index.index:
        dst_file = shutil.copyfile(sample['input'], os.path.join(sample_index.output_dir, sample['output']))
        files_collected.append(dst_file)

    return files_collected


def generate_sample_list(list_filename, files_collected):
    list_filename = os.path.abspath(os.path.expanduser(list_filename))

    try:
        fd = open(list_filename, 'w')
        for f in files_collected:
            fd.writelines("%s\n" % f)

        fd.close()
    except (FileExistsError, PermissionError):
        print_err("Could not create file list '%s'!" % list_filename)


def stdin_mode(target_cmd):
    return "@@" not in target_cmd


def generate_gdb_exploitable_script(script_filename, sample_index, target_cmd, script_id=0, intermediate=False):
    target_cmd = target_cmd.split()
    gdb_target_binary = target_cmd[0]
    gdb_run_cmd = " ".join(target_cmd[1:])

    if not intermediate:
        script_filename = os.path.abspath(os.path.expanduser(script_filename))
        print_ok("Generating final gdb+exploitable script '%s' for %d samples..." % (script_filename,
                                                                                     len(sample_index.outputs())))
    else:
        script_filename = os.path.abspath(os.path.expanduser("%s.%d" % (script_filename, script_id)))

    gdb_exploitable_path = None
    gdbinit = os.path.expanduser("~/.gdbinit")
    if not os.path.exists(gdbinit) or b"exploitable.py" not in open(gdbinit, "rb").read():
        gdb_exploitable_path = os.path.join(exploitable.__path__[0], "exploitable.py")

    try:
        fd = open(script_filename, 'w')

        # <script header>
        # source exploitable.py if necessary
        if gdb_exploitable_path:
            fd.writelines("source %s\n" % gdb_exploitable_path)

        # load executable
        fd.writelines("file %s\n" % gdb_target_binary)
        # </script_header>

        # fill script with content
        for f in sample_index.index:
            fd.writelines("echo Crash\ sample:\ '%s'\\n\n" % f['output'])  # noqa

            if not stdin_mode(target_cmd):
                run_cmd = "run " + gdb_run_cmd + "\n"
            else:
                run_cmd = "run " + gdb_run_cmd + "< @@" + "\n"

            if intermediate:
                run_cmd = run_cmd.replace("@@", "'{}'".format(f['input']))
            else:
                run_cmd = run_cmd.replace("@@", "'{}'".format(os.path.join(sample_index.output_dir, f['output'])))

            fd.writelines(run_cmd)
            fd.writelines("exploitable -m -s '{}' \n".format(f['output']))

        # <script_footer>
        fd.writelines("quit")
        # </script_footer>

        fd.close()
    except (FileExistsError, PermissionError):
        print_err("Could not open script file '%s' for writing!" % script_filename)


def execute_gdb_script(index, num_threads, commands):
    exploitable_path = os.path.join(exploitable.__path__[0], "exploitable.py")
    gdb_binary = shutil.which("gdb")
    in_queue = queue.Queue()

    classification_data = []

    queue_list = []
    thread_list = []

    # fill input queue with samples
    for s in index:
        in_queue.put(s)

    for n in range(0, num_threads, 1):
        commands['gdb_cmd'] = [
            str(gdb_binary),
            "-q",
            "--batch",
            "--ex", "source {}".format(exploitable_path),
            "--ex", "run",
        ]

        out_queue = queue.Queue()
        queue_list.append(out_queue)

        t = AflThread.ExecThread(n, 25, commands, in_queue, out_queue)
        thread_list.append(t)
        t.daemon = True
        t.start()

    in_queue.join()

    grepped_output = dict()
    asan_output = dict()
    lava_output = dict()

    for q in queue_list:
        while not q.empty():
            r = q.get()
            if 'classification' in r:
                grepped_output[r['sample_id']] = r
            if 'asan' in r:
                asan_output[r['sample_id']] = r['asan']
            if 'lava' in r:
                lava_output[r['sample_id']] = r['lava']

    for i in range(len(thread_list)):
        in_queue.put(None)
    for t in thread_list:
        t.join()

    missing = list()
    full_hash = set()
    print("*** GDB+EXPLOITABLE SCRIPT OUTPUT ***")
    for i, s in enumerate(index, 1):
        sample = s['output']
        if sample not in grepped_output:
            missing.append((i, sample))
            continue
        j = grepped_output[sample]
        if j['classification'] == "EXPLOITABLE":
            cex = clr.RED
            ccl = clr.BRI
        elif j['classification'] == "PROBABLY_EXPLOITABLE":
            cex = clr.YEL
            ccl = clr.BRI
        elif j['classification'] == "PROBABLY_NOT_EXPLOITABLE":
            cex = clr.BRN
            ccl = clr.RST
        elif j['classification'] == "NOT_EXPLOITABLE":
            cex = clr.GRN
            ccl = clr.GRA
        elif j['classification'] == "UNKNOWN":
            cex = clr.BLU
            ccl = clr.GRA
        else:
            cex = clr.GRA
            ccl = clr.GRA
        if j['hash'] in full_hash:
            cn = clr.GRA
        else:
            cn = clr.GRN
            full_hash.add(j['hash'])

        # Assume simplified sample file names,
        # so save some output space.
        ljust_width = 24 if len(j['sample_id']) < 24 else 64

        if sample in asan_output and 'stackhash' in asan_output[sample]:
            asan_hash = asan_output[sample]['stackhash']
            asan_summary = asan_output[sample]['summary']
        else:
            asan_hash = j['hash']
            asan_summary = ""

        bug_id = lava_output[sample]['bug_id'] if sample in lava_output else None
        s['bug_id'] = bug_id
        s['signal'] = j.get('signal')
        s['exploitability'] = j['classification']
        s['stack_hash'] = j['hash']
        s['additional'] = "{}\n{}".format(j['short_description'], j['fault_frame'])

        print("[%s%05d%s] %s: %s%s%s %s[%s]%s" % (cn, i, clr.RST, j['sample_id'].ljust(ljust_width, '.'), cex,
                                                  j['classification'], clr.RST, ccl, j['short_description'], clr.RST))
        classification_data.append({'Sample': j['sample_id'], 'Classification': j['classification'],
                                    'Classification_Description': j['short_description'], 'Hash': j['hash'],
                                    'MajorHash': j['major_hash'], 'FaultFrame': j['fault_frame'],
                                    'StackDepth': len(j['stack_frame']), 'Timestamp': datetime.datetime.now(),
                                    'User_Comment': "", 'Fuzzer': s['fuzzer'],
                                    'AsanHash': asan_hash, 'AsanSummary': asan_summary, 'LavaBug': bug_id})

    if len(missing) > 0:
        for i, sample in missing:
            ljust_width = 24 if len(sample) < 24 else 64
            print("%s[%05d]%s %s: no data for %s %s" %
                  (clr.GRA, i, clr.RST, sample.ljust(ljust_width, '.'), clr.LRD, clr.RST))
        print(clr.LRD + "Returned data may be incomplete!" + clr.RST)
    print("*** ***************************** ***")

    return classification_data


def main(argv=None):
    show_info("afl-collect", afl_utils.__author__,
              "Crash sample collection and processing utility for afl-fuzz.")
    check_gdb()

    parser = argparse.ArgumentParser(
        description=("afl-collect copies all crash sample files from an afl sync dir used"
                     " by multiple fuzzers when fuzzing in parallel into a single location providing"
                     " easy access for further crash analysis."))
    cfg_or_cmd = parser.add_mutually_exclusive_group(required=True)

    parser.add_argument("collection_dir",
                        help=("Output directory that will hold a copy of all crash samples and other generated files. "
                              "Existing files in the collection directory will be overwritten!"),
                        default='crashes_out')
    cfg_or_cmd.add_argument("-S", "--sync-dir", dest="sync_dir", default="outputs",
                            help="AFL sync-dir where fuzzer results are stored.")
    cfg_or_cmd.add_argument("-c", "--config", dest="config_file",
                            help="afl-multicore config file", default=None)
    parser.add_argument("-d", "--database", dest="database_file",
                        help=("Submit sample data into an sqlite3 database ( only when used together with '-e'). "
                              "afl-collect skips processing of samples already found in existing database."),
                        default=None)
    parser.add_argument("-e", "--execute-gdb-script", dest="gdb_expl_script_file",
                        help=("Generate and execute a gdb+exploitable script after crash sample collection for crash "
                              "classification. (Like option '-g', plus script execution.)"),
                        default=None)
    parser.add_argument("-f", "--filelist", dest="list_filename", default=None,
                        help="Writes all collected crash sample filenames into a file in the collection directory.")
    parser.add_argument("-g", "--generate-gdb-script", dest="gdb_script_file",
                        help=("Generate gdb script to run 'exploitable.py' on all collected crash samples. Generated "
                              "script will be placed into collection directory."), default=None)
    parser.add_argument("-j", "--threads", dest="num_threads", default=1, type=int,
                        help="Enable parallel analysis by specifying the number of threads afl-collect will utilize.")
    parser.add_argument("-m", "--minimize-filenames", dest="min_filename", action="store_const", const=True,
                        default=False, help="Minimize crash sample file names by only keeping fuzzer name and ID.")
    parser.add_argument("-r", "--remove-invalid", dest="remove_invalid", action="store_const", const=True,  # noqa
                        default=False,
                        help=("Verify collected crash samples and remove samples that do not lead to crashes or cause "
                              "timeouts (runs 'afl-vcrash.py -r' on collection directory). This step is done prior to "
                              "any script, file execution or file list generation."))
    parser.add_argument("-rr", "--remove-unexploitable", dest="remove_unexploitable", action="store_const", const=True,
                        default=False,
                        help=("Remove crash samples that have an exploitable classification of 'NOT_EXPLOITABLE' or "
                              " 'PROBABLY_NOT_EXPLOITABLE'. Sample file removal will take place after gdb+exploitable "
                              " script execution. Has no effect without '-e'."))
    parser.add_argument("-rt", "--remove-timeout", dest="remove_timeout", default=10,
                        help=("Specifies the maximum processing time in seconds for each sample during verification "
                              "phase. Samples that cause the target to run longer are marked as timeouts and are "
                              "removed from the index. Has no effect without '-r'."))
    parser.add_argument("target_cmd", nargs="*",
                        help=("Path to the target binary and its command line arguments.  Use '@@' to specify crash "
                              " sample input file position (see afl-fuzz usage)."), default=None)
    parser.add_argument("-s", "--submit", dest="submit", action="store_const", const=True,
                        default=False, help="Submit crashes to LuckyFuzz. (requires '.luckyfuzz' config file)")
    parser.add_argument("-F", "--force", dest="force", action="store_const", const=True,
                        default=False, help="Do not confirm new job creation to LuckyFuzz.")

    args = parser.parse_args(argv)
    if args.config_file is None:
        settings = {'output': args.sync_dir}
    else:
        settings = afl_multicore.read_config(args.config_file)

    sync_dir = settings['output']
    sync_dir = os.path.abspath(os.path.expanduser(sync_dir))
    if not os.path.exists(sync_dir):
        print_err("No valid directory provided for <SYNC_DIR>!")
        return

    out_dir = os.path.abspath(os.path.expanduser(args.collection_dir))
    results = {"crashes_in": 0, "crashes_out": 0, "invalid": 0, "timeout": 0, "major_remain": 0, "hash_remove": 0}
    if not os.path.exists(out_dir):
        os.makedirs(out_dir, exist_ok=True)

    if args.target_cmd:
        args.target_cmd = afl_multicore.build_target_cmd(args.target_cmd)
    else:
        args.target_cmd = afl_multicore.build_target_cmd(settings, alt_target=settings.get('drcov_target'))

    asan_cmd = None
    if 'asan_target' in settings:
        asan_cmd = afl_multicore.build_target_cmd(settings, alt_target=settings['asan_target'])

    if args.database_file:
        db_file = os.path.abspath(os.path.expanduser(args.database_file))
    else:
        db_file = ':memory:'

    # initialize database
    lite_db = con_sqlite.sqliteConnector(db_file)
    if not lite_db.init_database(db_table_spec) or \
            not lite_db.check_schema(new_columns):
        db_file = ':memory:'
        lite_db = con_sqlite.sqliteConnector(db_file)
        lite_db.init_database(db_table_spec)

    print_ok("Going to collect crash samples from '%s'." % sync_dir)
    fuzzers = get_fuzzer_instances(sync_dir)
    print_ok("Found %d fuzzers, collecting crash samples." % len(fuzzers))

    sample_index = build_sample_index(sync_dir, out_dir, fuzzers, lite_db, args.min_filename)

    if len(sample_index.index) > 0:
        results['crashes_in'] = len(sample_index.index)
        print_ok("Successfully indexed %d crash samples." % len(sample_index.index))
    elif db_file != ':memory:':
        print_warn("No unseen samples found. Check your database for results!")
        return
    else:
        print_warn("No samples found. Check directory settings!")
        return

    if args.remove_invalid:
        from afl_utils import afl_vcrash
        cmds = {'target_cmd': args.target_cmd, 'asan_cmd': asan_cmd}
        invalid_samples, timeout_samples = afl_vcrash.verify_samples(args.num_threads,
                                                                     sample_index.inputs(),
                                                                     cmds,
                                                                     timeout_secs=float(args.remove_timeout))

        # store invalid samples in db
        if args.gdb_expl_script_file:
            print_ok("Saving invalid sample info to database.")
            for sample in invalid_samples:
                sample_name = sample_index.outputs(input_file=sample)
                dataset = {'Sample': sample_name[0], 'Classification': 'INVALID',
                           'Classification_Description': 'Sample does not cause a crash in the target.', 'Hash': '',
                           'Timestamp': datetime.datetime.now(), 'User_Comment': ''}
                if not lite_db.dataset_exists(dataset, ['Sample']):
                    lite_db.insert_dataset(dataset)

            for sample in timeout_samples:
                sample_name = sample_index.outputs(input_file=sample)
                dataset = {'Sample': sample_name[0], 'Classification': 'TIMEOUT',
                           'Classification_Description': 'Sample caused a target execution timeout.', 'Hash': '',
                           'Timestamp': datetime.datetime.now(), 'User_Comment': ''}
                if not lite_db.dataset_exists(dataset, ['Sample']):
                    lite_db.insert_dataset(dataset)

        # remove invalid samples from sample index
        sample_index.remove_inputs(invalid_samples + timeout_samples)
        results['invalid'] = len(invalid_samples)
        results['timeout'] = len(timeout_samples)
        print_warn("Removed %d invalid crash samples from index." % len(invalid_samples))
        print_warn("Removed %d timed out samples from index." % len(timeout_samples))

    # generate gdb+exploitable script
    if args.gdb_expl_script_file:

        # execute gdb+exploitable script
        commands = {
            'target_cmd': args.target_cmd,
            'asan_cmd': asan_cmd
        }
        classification_data = execute_gdb_script(sample_index.index, args.num_threads, commands)
        seen = set(lite_db.select_unique('Hash'))

        # Submit crash classification data into database
        print_ok("Saving sample classification info to database.")
        for dataset in classification_data:
            lite_db.update_dataset(dataset, key='Sample')

        # de-dupe by exploitable hash and ASAN hash
        classification_data_dedupe = list()
        for x in classification_data:
            if x['Hash'] not in seen or x['AsanHash'] not in seen:
                seen.add(x['Hash'])
                seen.add(x['AsanHash'])
                classification_data_dedupe.append(x)

        # remove dupe samples identified by exploitable hash
        uninteresting_samples = [x['Sample'] for x in classification_data
                                 if x not in classification_data_dedupe]

        sample_index.remove_outputs(uninteresting_samples)

        results['hash_remove'] = len(uninteresting_samples)
        results['hash_remain'] = lite_db.count_unique('Hash')
        results['major_remain'] = lite_db.count_unique('MajorHash')
        results['asan_remain'] = lite_db.count_unique('AsanHash')
        results['asan_s_remain'] = lite_db.count_unique('AsanSummary')
        results['lava_bugs'] = lite_db.count_unique('LavaBug')
        print_warn("Removed %d duplicate samples from index. Will continue with %d remaining samples." %
                   (len(uninteresting_samples), len(sample_index.index)))
        print_warn("-- %d remaining samples if major hash used." % results['major_remain'])

        # remove crash samples that are classified uninteresting
        if args.remove_unexploitable:
            classification_unexploitable = [
                'NOT_EXPLOITABLE',
                'PROBABLY_NOT_EXPLOITABLE',
            ]

            uninteresting_samples = []

            for c in classification_data_dedupe:
                if c['Classification'] in classification_unexploitable:
                    uninteresting_samples.append(c['Sample'])

            sample_index.remove_outputs(uninteresting_samples)
            print_warn("Removed %d uninteresting crash samples from index." % len(uninteresting_samples))

        # generate output gdb script
        generate_gdb_exploitable_script(os.path.join(out_dir, args.gdb_expl_script_file), sample_index,
                                        args.target_cmd, 0)
    elif args.gdb_script_file:
        generate_gdb_exploitable_script(os.path.join(out_dir, args.gdb_script_file), sample_index, args.target_cmd)

    results['crashes_out'] = len(sample_index.index)
    save_json_data(results, os.path.join(sync_dir, 'crash_stats.json'))
    print_ok("Copying %d samples into output directory..." % len(sample_index.index))
    files_collected = copy_samples(sample_index)

    # optionally submit crash samples to API
    if args.submit:
        if args.force:
            settings['submit_force'] = args.force
        lf_client = LuckyFuzzClient(settings)
        if lf_client and lf_client.status:
            print_ok("Submitting %d samples to LuckyFuzz..." % len(sample_index.index))
            lf_client.submit_crashes(sample_index.index)

    # generate filelist of collected crash samples
    if args.list_filename:
        generate_sample_list(os.path.abspath(os.path.expanduser(args.list_filename)), files_collected)
        print_ok("Generated crash sample list '%s'." % os.path.abspath(os.path.expanduser(args.list_filename)))

    # write db contents to file and close db connection
    lite_db.commit_close()


if __name__ == "__main__":
    main(sys.argv)
