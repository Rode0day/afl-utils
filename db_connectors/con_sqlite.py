"""
Copyright 2015-2021 @_rc0r <hlt99@blinkenshell.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import os
import sqlite3 as lite

from afl_utils.AflPrettyPrint import print_warn, print_ok, print_err


class sqliteConnector:

    def __init__(self, database_path, table='Data', verbose=True):
        self.database_path = database_path
        self.dbcon = lite.connect(database_path, isolation_level='Exclusive')
        self.dbcur = self.dbcon.cursor()
        # self.dbcur.execute('PRAGMA synchronous = 0')
        # self.dbcur.execute('PRAGMA journal_mode = OFF')
        self.table = table
        self.verbose = verbose

    def init_database(self, table_spec, table='Data'):
        """
        Prepares a sqlite3 database for data set storage. If the file specified in database_path doesn't exist a new
        sqlite3 database with table 'Data' will be created. Otherwise the existing database is used to store additional
        data sets.

        DO NOT USE WITH USER SUPPLIED `table` AND `table_spec` PARAMS!
        !!! THIS METHOD IS *NOT* SQLi SAFE !!!

        :param table:       Name of the table to create.
        :param table_spec:  String containing the SQL table specification
        :return: None
        """
        table_data_exists = False
        if os.path.isfile(self.database_path):
            try:
                self.dbcur.execute("SELECT Count(*) FROM {}".format(table))
                if self.verbose:
                    print_warn("Using existing database to store results, %s entries in this database so far." %
                               str(self.dbcur.fetchone()[0]))
                table_data_exists = True
            except lite.OperationalError:
                if self.verbose:
                    print_warn("Table \'{}\' not found in existing database!".format(table))
            except lite.DatabaseError:
                print_err("Database error: database appears to be corrupt!")
                return False

        if not table_data_exists:   # If the database doesn't exist, we'll create it.
            if self.verbose:
                msg = "Creating new table \'{}\' in database \'{}\' to store data!".format(table, self.database_path)
                print_ok(msg)
            self.dbcur.execute("CREATE TABLE `{}` ({})".format(table, table_spec))
        return True

    def check_schema(self, columns, table='Data'):
        try:
            self.dbcur.execute("SELECT * FROM {} LIMIT 1".format(table))
            names = [d[0] for d in self.dbcur.description]
            for col in columns:
                if col[0] not in names:
                    stmt = "ALTER TABLE {} ADD COLUMN `{}` {}".format(table, col[0], col[1])
                    self.dbcur.execute(stmt)
                    self.dbcon.commit()
        except lite.Error:
            print_err("Database error: could not add the requested column.")
            return False
        return True

    def dataset_exists(self, dataset, compare_fields, table='Data'):
        """
        Check if dataset was already submitted into database.

        :param table:           Name of table to perform the check on.
        :param dataset:         A dataset dict consisting of sample filename, sample classification
                                and classification description.
        :param compare_fields:  List containing field names that will be checked using logical AND operation.
        :return:                True if the data set is already present in database, False otherwise.
        """
        # The nice thing about using the SQL DB is that I can just have it make
        # a query to make a duplicate check. This can likely be done better but
        # it's "good enough" for now.

        # check sample by its name (we could check by hash to avoid dupes in the db)
        compares = []
        for compare_field in compare_fields:
            compares.append("{}=:{}".format(compare_field, compare_field))

        qstring = "SELECT * FROM {} WHERE {}".format(table, " AND ".join(compares))
        self.dbcur.execute(qstring, dataset)
        return self.dbcur.fetchone() is not None  # We should only have to pull one.

    def insert_dataset(self, dataset, table='Data'):
        """
        Insert a dataset into the database.

        :param table:   Name of the table to insert data into.
        :param dataset: A dataset dict consisting of sample filename, sample classification and classification
                        description.
        :return:        None
        """
        # Just a simple function to write the results to the database.
        if len(dataset) <= 0:
            return

        field_names_string = ", ".join(["`{}`".format(k) for k in dataset.keys()])
        field_values_string = ", ".join([":{}".format(k) for k in dataset.keys()])
        qstring = "INSERT INTO {} ({}) VALUES({})".format(table, field_names_string, field_values_string)
        try:
            self.dbcur.execute(qstring, dataset)
        except lite.IntegrityError as e:
            print_warn("Sample already exists! \n\t ({})".format(e))
            return
        except lite.OperationalError as e:
            print_warn("Database not compatible! \n\t ({})".format(e))
            return

    def update_dataset(self, dataset, key='Sample', table='Data'):
        """
        Update a dataset in the database; insert if it doesn't exist.

        :param table:   Name of the table to insert data into.
        :param key:     Primary key for dataset update
        :param dataset: A dataset dict consisting of sample filename, sample classification and classification
                        description.
        :return:        None
        """
        if key not in dataset:
            print_warn("Attempted dataset update without primary key!")
            return

        if not self.dataset_exists(dataset, [key]):
            self.insert_dataset(dataset)
            return

        field_names = ", ".join(["`{}`=:{}".format(k, k) for k in dataset.keys() if k != key])
        qstring = "UPDATE {} SET ({}) WHERE {}=:{}".format(table, field_names, key, key)
        try:
            self.dbcur.execute(qstring, dataset)
        except lite.OperationalError as e:
            print_warn("Database not compatible! \n\t ({})".format(e))
            return

    def count_unique(self, key, table=None):
        table = self.table if table is None else table
        qstring = "SELECT COUNT(DISTINCT({})) FROM {}".format(key, table)
        try:
            self.dbcur.execute(qstring)
        except lite.OperationalError as e:
            print_warn("Database not compatible! \n\t ({})".format(e))
            return 0
        return self.dbcur.fetchone()[0]

    def select_unique(self, key, table=None):
        table = self.table if table is None else table
        qstring = "SELECT DISTINCT({}) FROM {}".format(key, table)
        try:
            self.dbcur.execute(qstring)
        except lite.OperationalError as e:
            print_warn("Database not compatible! \n\t ({})".format(e))
            return []

        return [x[0] for x in self.dbcur.fetchmany()]

    def commit_close(self):
        """
        Write database changes to disk and close cursor and connection.

        :return:    None
        """
        self.dbcon.commit()
        self.dbcur.close()
        self.dbcon.close()
